package game.peanutpanda.shooter.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

import game.peanutpanda.shooter.AudioManager;
import game.peanutpanda.shooter.GameData;
import game.peanutpanda.shooter.ScreenShake;
import game.peanutpanda.shooter.ai.Direction;
import game.peanutpanda.shooter.assetmanager.AssetLoader;
import game.peanutpanda.shooter.states.BattleState;
import game.peanutpanda.shooter.states.GameState;
import game.peanutpanda.shooter.states.ScreenSize;


public class Enemy extends MovingGameObject {

    Animation<TextureRegion> nukeAnimation;
    TextureAtlas nukeAtlas;
    private float oldX, oldY;
    private float degrees;
    private Animation<TextureRegion> moveAnimation;
    private boolean hit;
    private float hitPoints;
    private Direction direction;
    private int chanceToNuke = 0;
    private Rectangle nukeRect;
    private float expandTime;
    private boolean nuked;
    private boolean secondHandNuked;
    private boolean killedByPlayer;
    private boolean hasExploSoundPlayed;

    public Enemy(AssetLoader assetLoader, String moveAtlasLoc, float moveAnimSpeed, Rectangle rectangle) {
        this.sprite = new Sprite();
        TextureAtlas moveAtlas = assetLoader.manager.get(moveAtlasLoc, TextureAtlas.class);
        this.moveAnimation = new Animation<TextureRegion>(moveAnimSpeed, moveAtlas.getRegions());
        this.rectangle = rectangle;
    }

    public Enemy(AssetLoader assetLoader, Rectangle rectangle, int score, String moveAtlasLoc, float moveAnimSpeed) {
        this.rectangle = rectangle;
        this.score = score;
        this.sprite = new Sprite();
        this.visible = true;
        TextureAtlas moveAtlas = assetLoader.manager.get(moveAtlasLoc, TextureAtlas.class);
        this.destroyAtlas = assetLoader.manager.get("animations/explosions/exploBlock.atlas", TextureAtlas.class);
        this.moveAnimation = new Animation<TextureRegion>(moveAnimSpeed, moveAtlas.getRegions());
        this.destroyAnimation = new Animation<TextureRegion>(1f / 30f, destroyAtlas.getRegions());
        this.sprite.setOriginCenter();
        generateRandomDirection();

        this.nukeAtlas = assetLoader.manager.get("animations/explosions/nuke.atlas", TextureAtlas.class);
        this.nukeAnimation = new Animation<TextureRegion>(1f / 25f, nukeAtlas.getRegions());

        this.nukeRect = new Rectangle(0, 0, 0, 0);
    }

    private void generateRandomDirection() {
        direction = MathUtils.random(1, 2) == 1 ? Direction.LEFT : Direction.RIGHT;
    }

    @SuppressWarnings("SuspiciousNameCombination")
    public void update(SpriteBatch batch, Player player, GameState gameState, BattleState battleState,
                       AssetLoader assetLoader, float delta, ScreenShake screenShake, Array<Enemy> enemies,
                       Array<Block> blocks, Array<Rectangle> walls, Array<Rectangle> blockWalls, Rectangle floor,
                       GameData gameData, AudioManager audioManager) {

        oldX = rectangle.x;
        oldY = rectangle.y;

        if (!dead) {
            elapsedTime += delta;
            this.sprite.setRegion(moveAnimation.getKeyFrame(elapsedTime, true));
            double deltaX = player.getRectangle().x - rectangle.x;
            double deltaY = player.getRectangle().y - rectangle.y;
            degrees = (float) ((Math.atan2(deltaX, -(deltaY)) * 180.0d / Math.PI) + 90.0f);
            switch (battleState) {
                case LAND_BATTLE:
                    moveToOnLand(blocks, walls, blockWalls, floor);
                    break;
                case SPACE_BATTLE:
                    moveToInSpace();
                    break;
            }
        }

        if (rectangle.y < -100) {
            enemies.removeValue(this, true);
        }

        if (visible && gameState == GameState.IN_BATTLE) {
            batch.draw(sprite, rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        }

        if (hitPoints < 1) {
            showScore = true;
            dead = true;
        }

        if (collidesWithPlayer(player)) {
            killedByPlayer = true;
            showScore = true;
            dead = true;
        }

        if (dead && gameState == GameState.IN_BATTLE) {
            screenShake.shake(0.3f, 1, 35, 25, true);
            playDead(delta);
            if (!hasExploSoundPlayed) {
                audioManager.playEnemyBoom();
                hasExploSoundPlayed = true;
            }

            if (destroyAnimation == nukeAnimation) {
                nuked = true;
                if (!gameData.hugeExplosionsAreOn()) {
                    exploWidth = 200;
                    exploHeight = 200;
                    this.nukeRect = expandNukeBox(delta, exploWidth);
                    batch.draw(destroyAnimation.getKeyFrame(exploTime),
                            rectangle.x - (exploWidth / 3),
                            rectangle.y - 10,
                            exploWidth, exploHeight);
                } else {
                    exploWidth = 400;
                    exploHeight = 400;
                    this.nukeRect = expandNukeBox(delta, exploWidth / 2);
                    batch.draw(destroyAnimation.getKeyFrame(exploTime),
                            rectangle.x - (exploWidth / 2.8f),
                            rectangle.y - 10,
                            exploWidth, exploHeight);
                }

            } else {

                float x;
                float y;

                if (battleState == BattleState.SPACE_BATTLE) {
                    if (!gameData.hugeExplosionsAreOn()) {
                        exploWidth = 400;
                        exploHeight = 400;
                        x = rectangle.x - (exploWidth / 2);
                        y = rectangle.y - (exploHeight / 4);
                    } else {
                        exploWidth = 600;
                        exploHeight = 600;
                        x = rectangle.x - (exploWidth / 2.5f);
                        y = rectangle.y - (exploHeight / 4.5f);
                    }

                } else {

                    if (!gameData.hugeExplosionsAreOn()) {
                        exploWidth = 200;
                        exploHeight = 200;
                        x = rectangle.x - (exploWidth / 4);
                        y = rectangle.y - (exploHeight / 2);
                    } else {
                        exploWidth = 400;
                        exploHeight = 400;
                        x = rectangle.x - (exploWidth / 2);
                        y = rectangle.y - (exploHeight / 4);
                    }
                }

                batch.draw(destroyAnimation.getKeyFrame(exploTime),
                        x, y, exploWidth, exploHeight);
            }

            if (destroyAnimation.isAnimationFinished(exploTime)) {
                if (killedByPlayer && gameState == GameState.IN_BATTLE) {
                    batch.draw(assetLoader.redOverlay(), -100, -100, ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() * 2);
                    player.removeLife();
                }
                enemies.removeValue(this, true);
                exploTime = 0;
                player.setEnemiesKilled(player.getEnemiesKilled() + 1);
                if (!nuked && !secondHandNuked && gameState == GameState.IN_BATTLE) {
                    player.increaseScore(score);
                }
            }
        }
    }

    private Rectangle expandNukeBox(float delta, float exploWidth) {
        expandTime += delta * 5.0f;
        return new Rectangle(rectangle.x - expandTime * 30, rectangle.y, exploWidth * expandTime / 2.5f, rectangle.height * 2.3f);
    }

    public boolean collidesWithPlayer(Player player) {
        return collision.betweenRects(player.getRectangle(), rectangle)
                || collision.polyRect(player.getTurretPoly(), rectangle);
    }

    public void removeHitPoint(int hitPoint) {
        this.hitPoints -= hitPoint;
    }

    public boolean collidesWithBlock(Array<Block> blocks) {

        for (Block block : blocks) {
            if (collision.betweenRects(block.getRectangle(), rectangle) && block.isVisible()) {
                return true;
            }
        }

        return false;
    }

    public boolean collidesWithWall(Array<Rectangle> walls) {

        for (Rectangle wall : walls) {
            if (collision.betweenRects(wall, rectangle)) {
                return true;
            }
        }

        return false;
    }

    public boolean collidesWithFloor(Rectangle floor) {
        return collision.betweenRects(floor, rectangle);
    }

    private void turnAround() {

        if (direction == Direction.LEFT) {
            direction = Direction.RIGHT;
        } else {
            direction = Direction.LEFT;
        }
    }

    private void moveToInSpace() {
        float accumulator = 0;
        float frameTime = Math.min(Gdx.graphics.getDeltaTime(), 1 / 60f);
        accumulator += frameTime;
        while (accumulator >= 0.1) {
            accumulator -= 0.1;
        }
        movementCalculation.goDown(rectangle, MathUtils.random(speed * 150f, speed * 185f), accumulator);
    }

    private void moveToOnLand(Array<Block> blocks, Array<Rectangle> walls, Array<Rectangle> blockWalls, Rectangle floor) {

        float accumulator = 0;
        float frameTime = Math.min(Gdx.graphics.getDeltaTime(), 1 / 60f);
        accumulator += frameTime;
        while (accumulator >= 0.1) {
            accumulator -= 0.1;
        }

        for (Block block : blocks) {
            for (Rectangle blockWall : blockWalls) {
                if (block.getRectangle().contains(blockWall)) {
                    if (collision.betweenRects(blockWall, rectangle)) {
                        rectangle.x = oldX;
                        rectangle.y = oldY;
                        turnAround();
                    }
                }
            }
        }

        if (collidesWithWall(walls)) {
            rectangle.x = oldX;
            rectangle.y = oldY;
            turnAround();
        }

        if (collidesWithBlock(blocks) || collidesWithFloor(floor)) {
            rectangle.x = oldX;
            rectangle.y = oldY;

            if (chanceToNuke == 1) {
                destroyAnimation = nukeAnimation;
                dead = true;
            }

            if (direction == Direction.LEFT) {
                movementCalculation.goLeft(rectangle, speed * 35f, accumulator);
            } else if (direction == Direction.RIGHT) {
                movementCalculation.goRight(rectangle, speed * 35f, accumulator);
                sprite.setFlip(true, false);
            }
        } else {
            if (!dead) {
                chanceToNuke = MathUtils.random(1, 20);
                generateRandomDirection();
                movementCalculation.goDown(rectangle, speed * 210f, accumulator);
                if (direction == Direction.RIGHT) {
                    sprite.setFlip(true, false);
                }
            }
        }
    }

    public void setHitPoints(float hitPoints) {
        this.hitPoints = hitPoints;
    }

    public boolean isHit() {
        return hit;
    }

    public void setHit(boolean hit) {
        this.hit = hit;
    }

    public Rectangle getNukeRect() {
        return nukeRect;
    }

    public boolean isNuked() {
        return nuked;
    }

    public void setSecondHandNuked(boolean secondHandNuked) {
        this.secondHandNuked = secondHandNuked;
    }

    public Animation<TextureRegion> getMoveAnimation() {
        return moveAnimation;
    }
}