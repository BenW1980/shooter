package game.peanutpanda.shooter.gameobjects;


import game.peanutpanda.shooter.ai.MovementCalculation;

public class MovingGameObject extends GameObject {

    float numberOfTicks = 0f;
    float speed;
    float elapsedTime = 0f;
    MovementCalculation movementCalculation;

    MovingGameObject() {
        this.movementCalculation = new MovementCalculation();
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getNumberOfTicks() {
        return numberOfTicks;
    }

    public void setNumberOfTicks(float numberOfTicks) {
        this.numberOfTicks = numberOfTicks;
    }

}
