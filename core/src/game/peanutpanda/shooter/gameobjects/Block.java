package game.peanutpanda.shooter.gameobjects;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.utils.Array;

import game.peanutpanda.shooter.AudioManager;
import game.peanutpanda.shooter.GameData;
import game.peanutpanda.shooter.ScreenShake;
import game.peanutpanda.shooter.assetmanager.AssetLoader;

public class Block extends GameObject {

    private float hitPoints;
    private float maxHitPoints;
    private boolean hit;
    private float originalWidth;
    private float originalHeight;
    private float originalX;
    private float originalY;
    private boolean hasExploSoundPlayed;

    public Block(AssetLoader assetLoader, MapObject mapObject, Texture texture) {
        sprite = new Sprite(texture);
        rectangle = ((RectangleMapObject) mapObject).getRectangle();
        sprite.setBounds(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        this.visible = true;

        this.originalWidth = rectangle.width;
        this.originalHeight = rectangle.height;
        this.originalX = rectangle.x;
        this.originalY = rectangle.y;

        this.destroyAtlas = assetLoader.manager.get("animations/explosions/explo.atlas", TextureAtlas.class);
        this.destroyAnimation = new Animation<TextureRegion>(1f / 20f, destroyAtlas.getRegions());
    }

    public void removeHitPoint(float hitPoint) {
        this.hitPoints -= hitPoint;
    }

    public void update(AudioManager audioManager, SpriteBatch batch, Player player, Array<Block> blocks, ScreenShake screenShake, float delta, GameData gameData) {

        if (visible) {
            batch.draw(sprite, rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        }

        if (hitPoints < 1) {
            showScore = true;
            dead = true;
        }

        if (dead) {
            screenShake.shake(0.3f, 1, 35, 25, true);
            playDead(delta);
            if (!hasExploSoundPlayed) {
                audioManager.playBlockBoom();
                hasExploSoundPlayed = true;
            }

            if (!gameData.hugeExplosionsAreOn()) {
                exploWidth = rectangle.height * 3.0f; //80 width
                exploHeight = rectangle.width * 3.0f; //80 height
                batch.draw(destroyAnimation.getKeyFrame(exploTime),
                        rectangle.x - rectangle.width / 1.2f,
                        rectangle.y - rectangle.height / 1.6f,
                        exploWidth, exploHeight);
            } else {
                exploWidth = rectangle.height * 6.0f; //80 width
                exploHeight = rectangle.width * 6.0f; //80 height
                batch.draw(destroyAnimation.getKeyFrame(exploTime),
                        rectangle.x - rectangle.width / 0.45f,
                        rectangle.y - rectangle.height / 0.4f,
                        exploWidth, exploHeight);
            }


            if (destroyAnimation.isAnimationFinished(exploTime)) {
                blocks.removeValue(this, true);
                player.increaseScore(score);
                exploTime = 0;
            }
        }
    }

    public float getHitPoints() {
        return hitPoints;
    }

    public void setHitPoints(float hitPoints) {
        this.hitPoints = hitPoints;
    }

    public float getMaxHitPoints() {
        return maxHitPoints;
    }

    public void setMaxHitPoints(float maxHitPoints) {
        this.maxHitPoints = maxHitPoints;
    }

    public boolean isHit() {
        return hit;
    }

    public void setHit(boolean hit) {
        this.hit = hit;
    }

    public float getOriginalWidth() {
        return originalWidth;
    }

    public float getOriginalHeight() {
        return originalHeight;
    }

    public float getOriginalX() {
        return originalX;
    }

    public float getOriginalY() {
        return originalY;
    }

}