package game.peanutpanda.shooter.gameobjects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

import game.peanutpanda.shooter.Collision;
import game.peanutpanda.shooter.states.GameState;

public abstract class GameObject {

    Sprite sprite;
    Rectangle rectangle;
    Collision collision;
    float exploTime = 0f;
    boolean dead, visible;
    Animation<TextureRegion> destroyAnimation;
    TextureAtlas destroyAtlas;
    float exploHeight;
    float exploWidth;
    boolean showScore;
    int score;
    float scoreRisingTime;

    public GameObject() {
        this.collision = new Collision();
    }

    void playDead(float delta) {
        this.visible = false;
        this.exploTime += delta;
    }

    public void drawScore(float delta, BitmapFont font, SpriteBatch batch, float speed, GameState gameState) {
        if (showScore && gameState == GameState.IN_BATTLE) {
            scoreRisingTime += delta * speed;
            font.draw(batch, "+" + score, rectangle.x + rectangle.width / 2 - 20,
                    (rectangle.y + rectangle.height / 2) + scoreRisingTime);
        }
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public void setRectangle(Rectangle rectangle) {
        this.rectangle = rectangle;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }

    public boolean isDead() {
        return dead;
    }

    public void setDead(boolean dead) {
        this.dead = dead;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public float getExploTime() {
        return exploTime;
    }

    public void setExploTime(float exploTime) {
        this.exploTime = exploTime;
    }

    public boolean isShowScore() {
        return showScore;
    }

    public void setShowScore(boolean showScore) {
        this.showScore = showScore;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public float getScoreRisingTime() {
        return scoreRisingTime;
    }

    public void setScoreRisingTime(float scoreRisingTime) {
        this.scoreRisingTime = scoreRisingTime;
    }
}
