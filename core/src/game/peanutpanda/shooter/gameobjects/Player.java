package game.peanutpanda.shooter.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.Viewport;

import game.peanutpanda.shooter.AudioManager;
import game.peanutpanda.shooter.assetmanager.AssetLoader;
import game.peanutpanda.shooter.controller.Controller;
import game.peanutpanda.shooter.factories.ProjectileFactory;
import game.peanutpanda.shooter.factories.ProjectileType;
import game.peanutpanda.shooter.states.GameState;
import game.peanutpanda.shooter.states.ScreenSize;

public class Player extends GameObject {

    public static final int MAX_HEARTS_TO_ADD = 5;
    public static final int MAX_LIFE = 14;
    private SpriteBatch batch;
    private Array<Projectile> projectiles;
    private int enemiesKilled;
    private long lastShot;
    private ProjectileFactory projectileFactory;
    private ProjectileType projectileType;
    private Sprite turretSprite;
    private Sprite turretBase;
    private Rectangle turretRect;
    private Circle turretCircle;
    private Polygon turretPoly;
    private Array<Sprite> flashSpriteArray;
    private int projectileAmount;
    private float heartTime;
    private Array<Sprite> heartArray;
    private int skilLevel;
    private int timesHeartAdded;
    private boolean heartsFilledAfterStart;

    public Player(SpriteBatch batch, AssetLoader assLoader) {
        this.batch = batch;
        this.projectileFactory = new ProjectileFactory(assLoader, this);
        this.projectiles = new Array<Projectile>();
        this.turretSprite = new Sprite(assLoader.turret());
        this.turretBase = new Sprite(assLoader.turretBase());
        this.turretRect = new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 15, 1, 30, 110);
        this.turretSprite.setBounds(turretRect.x, turretRect.y, turretRect.width, turretRect.height);
        this.turretSprite.setOriginCenter();
        this.setProjectileType(ProjectileType.FIREBALL_1);

        this.turretCircle = new Circle(ScreenSize.WIDTH.getSize() / 2, 0, turretRect.height);
        this.rectangle = new Rectangle(ScreenSize.WIDTH.getSize() / 2 - turretRect.height / 2, 0, turretRect.height, turretRect.height / 2); // base
        this.turretPoly = new Polygon(new float[]{0, 0, turretRect.width, 0, turretRect.width, turretRect.height, 0, turretRect.height});

        this.turretPoly.setOrigin(turretRect.width / 2, 0);
        this.turretPoly.setPosition(turretRect.x, turretRect.y);

        this.flashSpriteArray = new Array<Sprite>();

        for (Texture t : assLoader.flashArray()) {
            Sprite s = new Sprite(t);
            flashSpriteArray.add(s);
        }

        this.heartArray = new Array<Sprite>();

        this.skilLevel = 1;
    }

    public void levelUp() {
        this.skilLevel++;
    }

    public void upgradeFirepower() {

        if (projectileType == ProjectileType.FIREBALL_1) {
            this.setProjectileType(ProjectileType.FIREBALL_2);

        } else if (projectileType == ProjectileType.FIREBALL_2) {
            this.setProjectileType(ProjectileType.FIREBALL_3);

        } else if (projectileType == ProjectileType.FIREBALL_3) {
            this.setProjectileType(ProjectileType.FIREBALL_4);

        } else if (projectileType == ProjectileType.FIREBALL_4) {
            this.setProjectileType(ProjectileType.FIREBALL_5);

        } else if (projectileType == ProjectileType.FIREBALL_5) {
            this.setProjectileType(ProjectileType.FIREBALL_6);

        } else if (projectileType == ProjectileType.FIREBALL_6) {
            this.setProjectileType(ProjectileType.FIREBALL_7);

        } else if (projectileType == ProjectileType.FIREBALL_7) {
            this.setProjectileType(ProjectileType.FIREBALL_8);
        }
    }

    public void fillHeartsAfterStart(AssetLoader assetLoader) {

        heartTime += Gdx.graphics.getDeltaTime();

        if (heartArray.size < MAX_LIFE) {

            if (heartArray.size > 0) {
                heartArray.get(heartArray.size - 1).setSize(28, 28);
            }
            if (heartTime > 0.05f) {
                heartArray.add(new Sprite(assetLoader.heart()));
                for (Sprite sprite : heartArray) {
                    sprite.setSize(20, 20);
                }
                heartTime = 0;
            }
        } else {
            heartsFilledAfterStart = true;
        }
    }

    public void fillHeartsAfterUpgrade(AssetLoader assetLoader) {

        heartTime += Gdx.graphics.getDeltaTime();

        if (heartArray.size < MAX_LIFE) {
            if (timesHeartAdded < MAX_HEARTS_TO_ADD) {

                if (heartArray.size > 0) {
                    heartArray.get(heartArray.size - 1).setSize(28, 28);
                }
                if (heartTime > 0.05f) {
                    heartArray.add(new Sprite(assetLoader.heart()));
                    for (Sprite sprite : heartArray) {
                        sprite.setSize(20, 20);
                    }
                    heartTime = 0;
                    timesHeartAdded++;
                }
            }
        } else {
            timesHeartAdded = MAX_HEARTS_TO_ADD;
        }
    }

    public void increaseScore(int amount) {
        score += amount;
    }

    public void removeLife() {
        if (heartArray.size > 0) {
            heartArray.removeIndex(heartArray.size - 1);
        }
    }

    private void drawMuzzleFlash(float degrees, Array<Vector2> turretPolyArray) {

        int flashWidth = 0, flashHeight = 0;

        switch (projectileAmount) {
            case 1:
                flashWidth = 100;
                flashHeight = 100;
                break;
            case 2:
                flashWidth = 120;
                flashHeight = 120;
                break;
            case 3:
                flashWidth = 140;
                flashHeight = 140;
                break;
        }

        batch.draw(flashSpriteArray.get(MathUtils.random(0, 2)),
                turretPolyArray.get(3).x - flashWidth / 2 + 8, turretPolyArray.get(3).y - 5,
                flashWidth / 2, 0,
                flashWidth, flashHeight,
                1, 1, degrees);
    }

    @SuppressWarnings("SuspiciousNameCombination")
    private void shoot(Viewport viewport, Controller controller, Array<Vector2> turretPolyArray) {
        Vector3 touchPos = new Vector3(controller.touchLocation, 0);
        viewport.getCamera().unproject(touchPos, viewport.getScreenX(),
                viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());

        double deltaX = touchPos.x - rectangle.x;
        double deltaY = touchPos.y - rectangle.y;
        double angle = Math.atan2(deltaY, deltaX);
        float degrees = (float) ((Math.atan2(deltaX, -(deltaY)) * 180.0d / Math.PI) - 180.0f);

        projectiles.addAll(projectileFactory.create(projectileType, angle, degrees));

        drawMuzzleFlash(degrees, turretPolyArray);

        lastShot = TimeUtils.nanoTime();
    }

    public void setProjectileType(ProjectileType projectileType) {
        this.projectileType = projectileType;
    }

    @SuppressWarnings("SuspiciousNameCombination")
    public void update(AudioManager audioManager, Viewport viewport, Controller controller, GameState gameState, Array<Vector2> turretPolyArray) {
        Vector3 touchPos = new Vector3(controller.touchLocation, 0);
        viewport.getCamera().unproject(touchPos, viewport.getScreenX(),
                viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());

        double deltaX = touchPos.x - rectangle.x;
        double deltaY = touchPos.y - rectangle.y;
        float degrees = (float) ((Math.atan2(deltaX, -(deltaY)) * 180.0d / Math.PI) - 180.0f);

        int shotTime = 60000000; // meer tijd tussen schoten -> shotTime verhogen (om sneller te schieten -> projectile.speed)
        if ((controller.isTouched || controller.isDragged) && gameState == GameState.IN_BATTLE) {

            this.turretPoly.setOrigin(turretRect.width / 2, 0);
            this.turretPoly.setPosition(turretRect.x, turretRect.y);
            this.turretPoly.setRotation(degrees);

            if (TimeUtils.nanoTime() - lastShot > shotTime) {
                shoot(viewport, controller, turretPolyArray);
                audioManager.playPlop();
            }
        }

        batch.draw(turretSprite, turretPoly.getX(), turretPoly.getY(), turretRect.width / 2, 0,
                turretRect.width, turretRect.height, 1, 1, degrees);

        batch.draw(turretBase, ScreenSize.WIDTH.getSize() / 2 - 80, -64, 160, 128);
    }

    public Array<Projectile> getProjectiles() {
        return projectiles;
    }

    public int getEnemiesKilled() {
        return enemiesKilled;
    }

    public void setEnemiesKilled(int enemiesKilled) {
        this.enemiesKilled = enemiesKilled;
    }

    public Rectangle getTurretRect() {
        return turretRect;
    }

    public Circle getTurretCircle() {
        return turretCircle;
    }

    public Polygon getTurretPoly() {
        return turretPoly;
    }

    public void setProjectileAmount(int projectileAmount) {
        this.projectileAmount = projectileAmount;
    }

    public Array<Sprite> getHeartArray() {
        return heartArray;
    }

    public int getSkilLevel() {
        return skilLevel;
    }

    public int getTimesHeartAdded() {
        return timesHeartAdded;
    }

    public void setTimesHeartAdded(int timesHeartAdded) {
        this.timesHeartAdded = timesHeartAdded;
    }

    public boolean isHeartsFilledAfterStart() {
        return heartsFilledAfterStart;
    }

    public void setHeartsFilledAfterStart(boolean heartsFilledAfterStart) {
        this.heartsFilledAfterStart = heartsFilledAfterStart;
    }
}