package game.peanutpanda.shooter.gameobjects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

import game.peanutpanda.shooter.assetmanager.AssetLoader;
import game.peanutpanda.shooter.states.ScreenSize;

public class Projectile extends MovingGameObject {

    private static final int MAX_COLLISION_COUNT = 2;
    private float oldX, oldY, oldPolyX, oldPolyY;
    private int damagePoints;
    private double angle; // schiethoek
    private float degrees; // richting hoek sprite
    private Polygon projectilePoly;
    private boolean collided;
    private int collisionCount = 0;

    public Projectile(Rectangle rectangle, float speed, int damagePoints, Sprite sprite, AssetLoader assetLoader, String destroyAtlasLoc, float destroyAnimSpeed) {

        this.rectangle = rectangle;
        this.sprite = sprite;
        this.sprite.setBounds(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        this.sprite.setOriginCenter();
        this.speed = speed;
        this.damagePoints = damagePoints;
        this.destroyAtlas = assetLoader.manager.get(destroyAtlasLoc, TextureAtlas.class);
        this.destroyAnimation = new Animation<TextureRegion>(destroyAnimSpeed, destroyAtlas.getRegions());
        this.visible = false;
        this.projectilePoly = new Polygon(new float[]{0, 0, rectangle.width, 0, rectangle.width, rectangle.height, 0, rectangle.height});
    }

    public void updateProjectilesForPlayer(Player player, SpriteBatch batch, float accumulator,
                                           Array<Block> blocks, Array<Enemy> enemies) {

        oldX = rectangle.x;
        oldY = rectangle.y;
        oldPolyX = projectilePoly.getX();
        oldPolyY = projectilePoly.getY();

        this.projectilePoly.setOrigin(rectangle.width / 2, 0);
        this.projectilePoly.setPosition(rectangle.x, rectangle.y);
        this.projectilePoly.setRotation(degrees);

        if (collidesWithBlock(blocks) || collidesWithEnemy(enemies) || collisionCount > MAX_COLLISION_COUNT) {
            rectangle.x = oldX;
            rectangle.y = oldY;
            projectilePoly.setPosition(oldPolyX, oldPolyY);
            dead = true;
        }

        if (isOutOfBounds()) {
            dead = true;
        }

        if (dead) {
            playDead(accumulator);
            player.getProjectiles().removeValue(this, true);
        }

        renderProjectilesForPlayer(player, batch, accumulator);
    }

    private void renderProjectilesForPlayer(Player player, SpriteBatch batch, float accumulator) {

        float speedUp;

        if ((rectangle.x < 0 - rectangle.width || rectangle.x > ScreenSize.WIDTH.getSize())
                && collisionCount < MAX_COLLISION_COUNT) {
            collisionCount++;
            collided = true;
            speed = -speed;
            angle = -angle;
            degrees = -degrees;
        }

        // Toon projectiel enkel opt moment het aan de bovenkant van de turret komt

        player.getTurretCircle().setRadius(player.getTurretRect().height - speed / 40);

        if (!collision.polyCir(projectilePoly, player.getTurretCircle()) || collided) {
            speedUp = 1;
            batch.draw(sprite, rectangle.x, rectangle.y,
                    rectangle.width / 2, 0,
                    rectangle.width, rectangle.height,
                    sprite.getScaleX(), sprite.getScaleY(), degrees);

        } else {
            speedUp = 5;
        }

        rectangle.x = (float) (rectangle.x + (speed * speedUp) * accumulator * Math.cos(angle));
        rectangle.y = (float) (rectangle.y + (speed * speedUp) * accumulator * Math.sin(angle));

    }

    public boolean collidesWithBlock(Array<Block> blocks) {

        for (Block block : blocks) {
            if (block.isVisible()) {
                if (collision.polyRect(projectilePoly, block.getRectangle())) {
                    block.setHit(true);
                    block.removeHitPoint(damagePoints);
                    return true;
                }
            }
        }

        return false;
    }

    public boolean collidesWithWall(Array<Rectangle> walls) {

        for (Rectangle wall : walls) {
            if (collision.betweenRects(wall, rectangle)) {
                return true;
            }
        }

        return false;
    }

    public boolean collidesWithEnemy(Array<Enemy> enemies) {

        for (Enemy enemy : enemies) {
            if (collision.polyRect(projectilePoly, enemy.getRectangle())) {
                enemy.setHit(true);
                enemy.removeHitPoint(damagePoints);
                return true;
            }
        }

        return false;
    }

    private boolean isOutOfBounds() {
        return rectangle.y > ScreenSize.HEIGHT.getSize() + 50
                || rectangle.x > ScreenSize.WIDTH.getSize() + 200
                || rectangle.x < -200
                || rectangle.y < 0;
    }

    public void resetPosition() {

    }

    public void setAngle(double angle) {
        this.angle = angle;
    }


    public int getDamagePoints() {
        return damagePoints;
    }

    public void setDegrees(float degrees) {
        this.degrees = degrees;
    }

    public Polygon getProjectilePoly() {
        return projectilePoly;
    }

}
