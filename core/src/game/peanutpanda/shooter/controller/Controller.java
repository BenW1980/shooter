package game.peanutpanda.shooter.controller;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;

public class Controller implements InputProcessor {

    public boolean isTouched, isDragged;
    public Vector2 touchLocation = new Vector2(0, 0);

    public Controller() {
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        isTouched = true;
        touchLocation.x = screenX;
        touchLocation.y = screenY;

        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        isDragged = false;
        isTouched = false;
        touchLocation.x = screenX;
        touchLocation.y = screenY;

        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        isDragged = true;
        touchLocation.x = screenX;
        touchLocation.y = screenY;
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
