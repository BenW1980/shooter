package game.peanutpanda.shooter.states;

public enum GameState {

    NEXT_LEVEL_MENU, IN_BATTLE, GAME_OVER, MAIN_MENU, CONFIRM_QUIT, PAUSED;

}
