package game.peanutpanda.shooter.states;

/**
 * Created by bw020_000 on 30/01/2018.
 */

public enum ScreenState {
    MAIN_MENU, IN_GAME, HIGHSCORE, OPTIONS, CREDITS, LOADING, TUTORIAL;
}
