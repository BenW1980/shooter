package game.peanutpanda.shooter.states;

public enum ScreenSize {

    HEIGHT(800), WIDTH(480); // TILED : W30/H50 - 16px/16px

    private final int size;

    ScreenSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

}