package game.peanutpanda.shooter.states;

public enum BattleState {

    LAND_BATTLE, SPACE_BATTLE;

}
