package game.peanutpanda.shooter.factories;


public enum EnemyType {

    GREY_SQUARE, HORNY, ORANGE, BROWN, GREEN, ROCKET, BULLET

}
