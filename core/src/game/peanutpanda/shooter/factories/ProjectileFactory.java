package game.peanutpanda.shooter.factories;


import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

import game.peanutpanda.shooter.assetmanager.AssetLoader;
import game.peanutpanda.shooter.gameobjects.Player;
import game.peanutpanda.shooter.gameobjects.Projectile;

public class ProjectileFactory {

    private AssetLoader assetLoader;
    private Player player;

    public ProjectileFactory(AssetLoader assetLoader, Player player) {
        this.assetLoader = assetLoader;
        this.player = player;
    }


    private Rectangle createLeft(float width, float height) {
        return new Rectangle(player.getTurretRect().x + (player.getTurretRect().width / 2 - width / 2) - 15,
                0, width, height);
    }

    private Rectangle createMiddle(float width, float height) {
        return new Rectangle(player.getTurretRect().x + (player.getTurretRect().width / 2 - width / 2),
                0, width, height);
    }

    private Rectangle createRight(float width, float height) {
        return new Rectangle(player.getTurretRect().x + (player.getTurretRect().width / 2 - width / 2) + 15,
                0, width, height);
    }

    private Rectangle createLeft2(float width, float height) {
        return new Rectangle(player.getTurretRect().x + (player.getTurretRect().width / 2 - width / 2) - 30,
                0, width, height);
    }

    private Rectangle createMiddle2(float width, float height) {
        return new Rectangle(player.getTurretRect().x + (player.getTurretRect().width / 2 - width / 2),
                0, width, height);
    }

    private Rectangle createRight2(float width, float height) {
        return new Rectangle(player.getTurretRect().x + (player.getTurretRect().width / 2 - width / 2) + 30,
                0, width, height);
    }

    public Array<Projectile> create(ProjectileType type, double angle, float degrees) {

        Array<Projectile> array = new Array<Projectile>();
        int width, height;

        switch (type) {
            case FIREBALL_1:
                player.setProjectileAmount(1);
                width = 20;
                height = 75;
                array.add(new Projectile(createMiddle(width, height),
                        1500f, 1, new Sprite(assetLoader.fireball()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                break;

            case FIREBALL_2:
                player.setProjectileAmount(2);
                width = 20;
                height = 75;

                array.add(new Projectile(createLeft(width, height),
                        1500f, 1, new Sprite(assetLoader.fireball()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                array.add(new Projectile(createRight(width, height),
                        1500f, 1, new Sprite(assetLoader.fireball()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                break;

            case FIREBALL_3:
                player.setProjectileAmount(3);
                width = 20;
                height = 75;
                array.add(new Projectile(createLeft(width, height),
                        1500f, 1, new Sprite(assetLoader.fireball()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                array.add(new Projectile(createMiddle(width, height),
                        1500f, 1, new Sprite(assetLoader.fireball()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                array.add(new Projectile(createRight(width, height),
                        1500f, 1, new Sprite(assetLoader.fireball()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                break;
            case FIREBALL_4:
                player.setProjectileAmount(3);
                width = 30;
                height = 65;
                array.add(new Projectile(createLeft2(width, height),
                        1500f, 2, new Sprite(assetLoader.fireballRed()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                array.add(new Projectile(createMiddle2(width, height),
                        1500f, 1, new Sprite(assetLoader.fireballRed()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                array.add(new Projectile(createRight2(width, height),
                        1500f, 2, new Sprite(assetLoader.fireballRed()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                break;
            case FIREBALL_5:
                player.setProjectileAmount(3);
                width = 30;
                height = 65;
                array.add(new Projectile(createLeft2(width, height),
                        1500f, 2, new Sprite(assetLoader.fireballRed()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                array.add(new Projectile(createMiddle2(width, height),
                        1500f, 1, new Sprite(assetLoader.fireballRed()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                array.add(new Projectile(createRight2(width, height),
                        1500f, 2, new Sprite(assetLoader.fireballRed()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));

                array.add(new Projectile(createLeft(20, height),
                        1500f, 1, new Sprite(assetLoader.fireball()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                array.add(new Projectile(createRight(20, height),
                        1500f, 1, new Sprite(assetLoader.fireball()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                break;
            case FIREBALL_6:
                player.setProjectileAmount(3);
                width = 100;
                height = 75;
                array.add(new Projectile(createLeft2(width, height),
                        1500f, 3, new Sprite(assetLoader.fireballGreen()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                array.add(new Projectile(createMiddle2(width, height),
                        1500f, 2, new Sprite(assetLoader.fireballGreen()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                array.add(new Projectile(createRight2(width, height),
                        1500f, 3, new Sprite(assetLoader.fireballGreen()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                break;

            case FIREBALL_7:
                player.setProjectileAmount(3);
                width = 100;
                height = 75;
                array.add(new Projectile(createLeft2(width, height),
                        1500f, 2, new Sprite(assetLoader.fireballGreen()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                array.add(new Projectile(createMiddle2(width, height),
                        1500f, 2, new Sprite(assetLoader.fireballGreen()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                array.add(new Projectile(createRight2(width, height),
                        1500f, 2, new Sprite(assetLoader.fireballGreen()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));

                array.add(new Projectile(createLeft2(30, 65),
                        1500f, 1, new Sprite(assetLoader.fireballRed()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                array.add(new Projectile(createMiddle2(30, 65),
                        1500f, 2, new Sprite(assetLoader.fireballRed()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                array.add(new Projectile(createRight2(30, 65),
                        1500f, 1, new Sprite(assetLoader.fireballRed()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                break;

            case FIREBALL_8:
                player.setProjectileAmount(3);
                width = 100;
                height = 75;
                array.add(new Projectile(createLeft2(width, height),
                        1500f, 3, new Sprite(assetLoader.fireballGreen()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                array.add(new Projectile(createMiddle2(width, height),
                        1500f, 2, new Sprite(assetLoader.fireballGreen()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                array.add(new Projectile(createRight2(width, height),
                        1500f, 3, new Sprite(assetLoader.fireballGreen()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));

                array.add(new Projectile(createLeft2(30, 65),
                        1500f, 1, new Sprite(assetLoader.fireballRed()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                array.add(new Projectile(createMiddle2(30, 65),
                        1500f, 2, new Sprite(assetLoader.fireballRed()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                array.add(new Projectile(createRight2(30, 65),
                        1500f, 1, new Sprite(assetLoader.fireballRed()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));

                array.add(new Projectile(createLeft(20, 75),
                        1500f, 1, new Sprite(assetLoader.fireball()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                array.add(new Projectile(createRight(20, 75),
                        1500f, 1, new Sprite(assetLoader.fireball()),
                        assetLoader, "animations/explosions/explo.atlas", 1f / 30f));
                break;
        }

        for (Projectile p : array) {
            p.setAngle(angle);
            p.setDegrees(degrees);
        }

        return array;
    }
}
