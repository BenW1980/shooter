package game.peanutpanda.shooter.factories;

import com.badlogic.gdx.maps.MapObject;

import game.peanutpanda.shooter.assetmanager.AssetLoader;
import game.peanutpanda.shooter.gameobjects.Block;

public class BlockFactory {

    private AssetLoader assetLoader;

    public BlockFactory(AssetLoader assetLoader) {
        this.assetLoader = assetLoader;
    }

    public Block createRandom(MapObject mapObject, int i, float levelCount) {

        BlockType type = null;

        switch (i) {
            case 1:
                type = BlockType.GREEN_ROCK;
                break;
            case 2:
                type = BlockType.BROWN_ROCK;
                break;
            case 3:
                type = BlockType.PURPLE_ROCK;
                break;
            case 4:
                type = BlockType.RED_ROCK;
                break;
            case 5:
                type = BlockType.YELLOW_ROCK;
                break;
            case 6:
                type = BlockType.ORANGE_ROCK;
                break;
            case 7:
                type = BlockType.BLUE_ROCK;
                break;
        }

        return create(mapObject, type, levelCount);

    }

    public Block create(MapObject mapObject, BlockType type, float levelCount) {

        Block block = null;

        switch (type) {
            case GREEN_ROCK:
                block = new Block(assetLoader, mapObject, assetLoader.greenRock());
                break;
            case BROWN_ROCK:
                block = new Block(assetLoader, mapObject, assetLoader.brownRock());
                break;
            case PURPLE_ROCK:
                block = new Block(assetLoader, mapObject, assetLoader.purpleRock());
                break;
            case RED_ROCK:
                block = new Block(assetLoader, mapObject, assetLoader.redRock());
                break;
            case YELLOW_ROCK:
                block = new Block(assetLoader, mapObject, assetLoader.yellowRock());
                break;
            case ORANGE_ROCK:
                block = new Block(assetLoader, mapObject, assetLoader.orangeRock());
                break;
            case BLUE_ROCK:
                block = new Block(assetLoader, mapObject, assetLoader.blueRock());
                break;
        }


//        switch (type) {
//            case GREEN_ROCK:
//                block = new Block(assetLoader, mapObject, assetLoader.greenRock());
//                break;
//            case BROWN_ROCK:
//                block = new Block(assetLoader, mapObject, assetLoader.brownRock());
//                break;
//            case PURPLE_ROCK:
//                block = new Block(assetLoader, mapObject, assetLoader.purpleRock());
//                break;
//            case RED_ROCK:
//                block = new Block(assetLoader, mapObject, assetLoader.redRock());
//                break;
//            case YELLOW_ROCK:
//                block = new Block(assetLoader, mapObject, assetLoader.yellowRock());
//                break;
//            case ORANGE_ROCK:
//                block = new Block(assetLoader, mapObject, assetLoader.orangeRock());
//                break;
//            case BLUE_ROCK:
//                block = new Block(assetLoader, mapObject, assetLoader.blueRock());
//                break;
//        }


        if (levelCount <= 7) {
            block.setHitPoints(5);
        } else if (levelCount > 7 && levelCount <= 14) {
            block.setHitPoints(levelCount + 1);
        } else if (levelCount > 14 && levelCount <= 21) {
            block.setHitPoints(levelCount * 1.1f);
        } else {
            block.setHitPoints(levelCount * 1.3f);
        }

        block.setMaxHitPoints(block.getHitPoints());

        block.setScore((int) (450 + (levelCount * 50)));
        return block;
    }
}
