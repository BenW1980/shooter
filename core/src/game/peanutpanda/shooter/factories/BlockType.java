package game.peanutpanda.shooter.factories;

/**
 * Created by bw020_000 on 3/01/2018.
 */

public enum BlockType {

    GREEN_ROCK, BROWN_ROCK, PURPLE_ROCK, RED_ROCK, YELLOW_ROCK, ORANGE_ROCK, BLUE_ROCK
}
