package game.peanutpanda.shooter.factories;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

import game.peanutpanda.shooter.assetmanager.AssetLoader;
import game.peanutpanda.shooter.gameobjects.Enemy;
import game.peanutpanda.shooter.states.ScreenSize;

public class EnemyFactory {

    private AssetLoader assetLoader;

    public EnemyFactory(AssetLoader assetLoader) {
        this.assetLoader = assetLoader;

    }

    public Enemy createSpaceEnemy(EnemyType type, float levelCount) {

        Enemy enemy = null;

        switch (type) {
            case ROCKET:
                enemy = new Enemy(assetLoader, new Rectangle(
                        MathUtils.random(50, ScreenSize.WIDTH.getSize() - 50),
                        MathUtils.random(ScreenSize.HEIGHT.getSize() + 50, ScreenSize.HEIGHT.getSize() + 200),
                        65, 100), (int) (levelCount * 100),
                        "animations/enemies/rocket.atlas", 1f / 15f
                );
                break;
            case BULLET:
                enemy = new Enemy(assetLoader, new Rectangle(
                        MathUtils.random(50, ScreenSize.WIDTH.getSize() - 50),
                        MathUtils.random(ScreenSize.HEIGHT.getSize() + 50, ScreenSize.HEIGHT.getSize() + 200),
                        53, 100), (int) (levelCount * 100),
                        "animations/enemies/bullet.atlas", 1f / 15f
                );
                break;
        }

        if (levelCount == 7) {
            enemy.setHitPoints(4);
        } else if (levelCount == 14) {
            enemy.setHitPoints(8);
        } else if (levelCount == 21) {
            enemy.setHitPoints(12);
        } else {
            enemy.setHitPoints(levelCount / 2 + 3.0f);
        }

        enemy.setSpeed(2.5f);
        return enemy;
    }

    public Enemy create(EnemyType type, float x, float levelCount) {

        Enemy enemy = null;
        float minSpeed = 2.5f;
        float maxSpeed = 2.7f;
        int height = MathUtils.random(40, 70);

        switch (type) {
            case GREY_SQUARE:
                enemy = new Enemy(assetLoader, new Rectangle(
                        x,
                        ScreenSize.HEIGHT.getSize() + height,
                        40, 40), 10,
                        "animations/enemies/monster.atlas", 1f / 300f
                );
                enemy.setSpeed(MathUtils.random(minSpeed, maxSpeed));
                break;
            case ORANGE:
                enemy = new Enemy(assetLoader, new Rectangle(
                        x,
                        ScreenSize.HEIGHT.getSize() + height,
                        40, 40), 25,
                        "animations/enemies/orange.atlas", 1f / 300f
                );
                enemy.setSpeed(MathUtils.random(minSpeed, maxSpeed));
                break;
            case GREEN:
                enemy = new Enemy(assetLoader, new Rectangle(
                        x,
                        ScreenSize.HEIGHT.getSize() + height,
                        40, 40), 20,
                        "animations/enemies/green.atlas", 1f / 300f
                );
                enemy.setSpeed(MathUtils.random(minSpeed, maxSpeed));
                break;
            case HORNY:
                enemy = new Enemy(assetLoader, new Rectangle(
                        x,
                        ScreenSize.HEIGHT.getSize() + height,
                        40, 40), 30,
                        "animations/enemies/horny.atlas", 1f / 200f
                );
                enemy.setSpeed(MathUtils.random(minSpeed, maxSpeed));
                break;
            case BROWN:
                enemy = new Enemy(assetLoader, new Rectangle(
                        x,
                        ScreenSize.HEIGHT.getSize() + height,
                        40, 40), 15,
                        "animations/enemies/brown.atlas", 1f / 30f
                );
                enemy.setSpeed(MathUtils.random(minSpeed, maxSpeed));

                break;
        }

        if (levelCount <= 7) {
            enemy.setHitPoints(1);
        } else if (levelCount > 7 && levelCount <= 12) {
            enemy.setHitPoints(2);
        } else if (levelCount > 12 && levelCount <= 19) {
            enemy.setHitPoints(2);
        } else {
            enemy.setHitPoints(3);
            enemy.setSpeed(enemy.getSpeed() * 1.15f);
        }
        enemy.setScore((int) (enemy.getScore() + (levelCount * 5)));

        return enemy;
    }
}