package game.peanutpanda.shooter;


import com.badlogic.gdx.math.MathUtils;

import game.peanutpanda.shooter.assetmanager.AssetLoader;

public class AudioManager {

    private GameData gameData;
    private AssetLoader assetLoader;

    public AudioManager(GameData gameData, AssetLoader assetLoader) {
        this.gameData = gameData;
        this.assetLoader = assetLoader;
    }

    public void playMusic() {
        if (gameData.musicIsOn()) {
            assetLoader.gameMusic().play();
            assetLoader.gameMusic().setVolume(0.5f);
            assetLoader.gameMusic().setLooping(true);
        } else {
            assetLoader.gameMusic().stop();
        }
    }

    public void playPlop() {
        if (gameData.soundIsOn()) {
            assetLoader.plop().setPitch(assetLoader.plop().play(0.25f), MathUtils.random(0.5f, 2.0f));
        }
    }

    public void playBlockBoom() {
        if (gameData.soundIsOn()) {
            assetLoader.boom().setPitch(assetLoader.boom().play(0.7f), MathUtils.random(0.5f, 2.0f));
        }
    }

    public void playEnemyBoom() {
        if (gameData.soundIsOn()) {
            assetLoader.boom().setPitch(assetLoader.boom().play(0.6f), MathUtils.random(1.0f, 2.0f));
        }
    }

    public void playDeath() {
        if (gameData.soundIsOn()) {
            assetLoader.death().play();
        }
    }

    public void playJingle() {
        if (gameData.soundIsOn()) {
            assetLoader.jingle().play();
        }

    }

    public void playBtnSound() {
        if (gameData.soundIsOn()) {
            assetLoader.ping().play();
        }
    }

    public void playTglSound() {
        if (gameData.soundIsOn()) {
            assetLoader.tick().play();
        }
    }
}
