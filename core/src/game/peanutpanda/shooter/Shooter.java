package game.peanutpanda.shooter;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import game.peanutpanda.shooter.assetmanager.AssetLoader;
import game.peanutpanda.shooter.controller.Controller;
import game.peanutpanda.shooter.screens.BattleScreen;
import game.peanutpanda.shooter.screens.CreditsScreen;
import game.peanutpanda.shooter.screens.HighScoreScreen;
import game.peanutpanda.shooter.screens.LoadingScreen;
import game.peanutpanda.shooter.screens.MainMenuScreen;
import game.peanutpanda.shooter.screens.OptionsScreen;
import game.peanutpanda.shooter.screens.TutorialScreen;
import game.peanutpanda.shooter.states.BattleState;
import game.peanutpanda.shooter.states.GameState;
import game.peanutpanda.shooter.states.ScreenSize;
import game.peanutpanda.shooter.states.ScreenState;

public class Shooter extends Game {
    public AssetLoader assLoader;
    public OrthographicCamera camera;
    public Viewport viewport;
    public SpriteBatch batch;
    public SpriteBatch shaderBatch;
    public Controller controller;
    public GameState gameState;
    public BattleState battleState;
    public GameData gameData;
    public AudioManager audioManager;

    @Override
    public void create() {
        this.assLoader = new AssetLoader();
//        this.assLoader.loadFontHandling();
//        this.assLoader.loadImages();
//        this.assLoader.loadAtlas();
//        this.assLoader.loadSounds();
//        this.assLoader.loadMusic();
//        this.assLoader.loadTiledMaps();
//        this.assLoader.manager.finishLoading();

        this.batch = new SpriteBatch();
        this.shaderBatch = new SpriteBatch();
        this.controller = new Controller();
        this.gameData = new GameData();
        this.audioManager = new AudioManager(gameData, assLoader);
        this.camera = new OrthographicCamera();
        this.camera.setToOrtho(false, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        this.viewport = new FitViewport(ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize(), camera);
        this.camera.update();

        this.changeScreen(ScreenState.LOADING);
    }

    public void changeScreen(ScreenState screenState) {

        switch (screenState) {
            case MAIN_MENU:
                this.setScreen(new MainMenuScreen(this));
                break;
            case IN_GAME:
                this.setScreen(new BattleScreen(this));
                break;
            case HIGHSCORE:
                this.setScreen(new HighScoreScreen(this));
                break;
            case OPTIONS:
                this.setScreen(new OptionsScreen(this));
                break;
            case CREDITS:
                this.setScreen(new CreditsScreen(this));
                break;
            case LOADING:
                this.setScreen(new LoadingScreen(this));
                break;
            case TUTORIAL:
                this.setScreen(new TutorialScreen(this));
                break;
        }
    }

    @Override
    public void dispose() {

    }
}
