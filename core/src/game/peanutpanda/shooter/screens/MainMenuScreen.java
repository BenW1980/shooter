package game.peanutpanda.shooter.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

import game.peanutpanda.shooter.Shooter;
import game.peanutpanda.shooter.states.ScreenSize;
import game.peanutpanda.shooter.states.ScreenState;
import game.peanutpanda.shooter.ui.Button;


public class MainMenuScreen extends GameScreen {

    private Texture title;
    private Button startButton, optionsButton, highScoreButton;

    public MainMenuScreen(Shooter parent) {
        super(parent);
        Gdx.input.setCatchBackKey(false);
        title = assetLoader.title();
        this.initGrass(MathUtils.random(1, 3));
        this.randomiseBackground();
        int btnSize = 90;

        this.optionsButton = new Button(controller, viewport, batch,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 165, ScreenSize.HEIGHT.getSize() / 2 - 220, btnSize, btnSize),
                assetLoader.optionsUnclicked(), assetLoader.optionsClicked());

        this.startButton = new Button(controller, viewport, batch,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 50, ScreenSize.HEIGHT.getSize() / 2 - 230, btnSize + 20, btnSize + 20),
                assetLoader.nextUnclicked(), assetLoader.nextClicked());

        this.highScoreButton = new Button(controller, viewport, batch,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 85, ScreenSize.HEIGHT.getSize() / 2 - 220, btnSize, btnSize),
                assetLoader.highScoreUnclicked(), assetLoader.highScoreClicked());

    }

    @Override
    public void render(float delta) {
        super.render(delta);
        batch.begin();
        batch.draw(title, 40, 480, 400, 300);

        for (Sprite sprite : grassSprites) {
            sprite.draw(batch);
        }

        startButton.draw(audioManager);
        optionsButton.draw(audioManager);
        highScoreButton.draw(audioManager);

        batch.end();

        if (startButton.isProcessed()) {
            parent.changeScreen(ScreenState.TUTORIAL);
        }

        if (highScoreButton.isProcessed()) {
            parent.changeScreen(ScreenState.HIGHSCORE);
        }

        if (optionsButton.isProcessed()) {
            parent.changeScreen(ScreenState.OPTIONS);
        }

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
