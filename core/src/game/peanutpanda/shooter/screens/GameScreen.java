package game.peanutpanda.shooter.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;
import aurelienribon.tweenengine.equations.Sine;
import game.peanutpanda.shooter.AudioManager;
import game.peanutpanda.shooter.GameData;
import game.peanutpanda.shooter.ScreenShake;
import game.peanutpanda.shooter.Shooter;
import game.peanutpanda.shooter.SpriteAccessor;
import game.peanutpanda.shooter.assetmanager.AssetLoader;
import game.peanutpanda.shooter.controller.Controller;
import game.peanutpanda.shooter.states.BattleState;
import game.peanutpanda.shooter.states.GameState;
import game.peanutpanda.shooter.states.ScreenSize;


public abstract class GameScreen implements Screen {

    SpriteBatch batch;
    SpriteBatch shaderBatch;
    BitmapFont fontNobile;
    BitmapFont fontNobileLarge;
    BitmapFont fontNobileMedium;
    BitmapFont fontWolf;
    GameState gameState;
    BattleState battleState;
    OrthographicCamera camera;
    Viewport viewport;
    Controller controller;
    AssetLoader assetLoader;
    ScreenShake screenShake;
    Shooter parent;

    Texture background;
    Texture spaceBackground;
    Array<Texture> backgroundArray;
    Array<Texture> spaceBackgroundArray;
    Sprite grassSprite1;
    Sprite grassSprite2;
    Sprite grassSprite3;
    Array<Sprite> grassSprites;
    AudioManager audioManager;
    GameData gameData;

    TweenManager tweenManager = new TweenManager();
    final TweenCallback windCallback = new TweenCallback() {
        @Override
        public void onEvent(int type, BaseTween<?> source) {
            float d = MathUtils.random() * 0.5f + 0.5f;    // duration
            float t = -0.5f * grassSprite1.getHeight();    // amplitude

            Timeline.createParallel()
                    .push(Tween.to(grassSprite1, SpriteAccessor.SKEW_X2X3, d).target(t, t).ease(Sine.INOUT).repeatYoyo(1, 0).setCallback(windCallback))
                    .push(Tween.to(grassSprite2, SpriteAccessor.SKEW_X2X3, d).target(t, t).ease(Sine.INOUT).delay(d / 3).repeatYoyo(1, 0))
                    .push(Tween.to(grassSprite3, SpriteAccessor.SKEW_X2X3, d).target(t, t).ease(Sine.INOUT).delay(d / 3 * 2).repeatYoyo(1, 0))
                    .start(tweenManager);
        }
    };


    GameScreen(Shooter parent) {
        this.parent = parent;
        this.batch = parent.batch;
        this.shaderBatch = parent.shaderBatch;
        this.controller = parent.controller;
        this.fontNobile = parent.assLoader.manager.get("Nobile-Bold.ttf", BitmapFont.class);
        this.fontNobileLarge = parent.assLoader.manager.get("Nobile-BoldLarge.ttf", BitmapFont.class);
        this.fontNobileMedium = parent.assLoader.manager.get("Nobile-BoldMedium.ttf", BitmapFont.class);
        this.fontWolf = parent.assLoader.manager.get("wolfsbane2.ttf", BitmapFont.class);
        this.camera = parent.camera;
        this.viewport = parent.viewport;
        this.assetLoader = parent.assLoader;
        this.gameState = parent.gameState;
        this.battleState = parent.battleState;
        this.gameData = parent.gameData;
        this.audioManager = parent.audioManager;
        this.camera.update();
        this.screenShake = new ScreenShake();
        this.backgroundArray = assetLoader.backgroundArray();
        this.spaceBackgroundArray = assetLoader.spaceBackgroundArray();

        Gdx.input.setInputProcessor(this.controller);

    }

    public void randomiseBackground() {
        int backgroundNumber = MathUtils.random(0, backgroundArray.size - 1);
        int spadeBackgroundNumber = MathUtils.random(0, spaceBackgroundArray.size - 1);
        background = backgroundArray.get(backgroundNumber);
        spaceBackground = spaceBackgroundArray.get(spadeBackgroundNumber);
    }

    public void initGrass(int random) {

        switch (random) {
            case 1:
                grassSprite1 = new Sprite(assetLoader.grass1());
                grassSprite2 = new Sprite(assetLoader.grass2());
                grassSprite3 = new Sprite(assetLoader.grass3());
                break;
            case 2:
                grassSprite1 = new Sprite(assetLoader.grassBlue1());
                grassSprite2 = new Sprite(assetLoader.grassBlue2());
                grassSprite3 = new Sprite(assetLoader.grassBlue3());
                break;
            case 3:
                grassSprite1 = new Sprite(assetLoader.grassPink1());
                grassSprite2 = new Sprite(assetLoader.grassPink2());
                grassSprite3 = new Sprite(assetLoader.grassPink3());
                break;
        }

        this.grassSprites = new Array<Sprite>();
        this.grassSprites.addAll(grassSprite1, grassSprite2, grassSprite3);
        for (Sprite sprite : grassSprites) {
            sprite.setSize(ScreenSize.WIDTH.getSize() + 35, 55);
            sprite.setPosition(-5, -20);
        }

        Tween.registerAccessor(Sprite.class, new SpriteAccessor());
        Tween.call(windCallback).start(tweenManager);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        batch.setProjectionMatrix(camera.combined);
        shaderBatch.setProjectionMatrix(camera.combined);

        tweenManager.update(delta);

        batch.begin();
        batch.draw(background, 0, 0, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        batch.end();

        if (gameState == GameState.IN_BATTLE && gameData.screenShakeIsOn()) {
            screenShake.update(delta, camera);
        }

        audioManager.playMusic();

    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        shaderBatch.dispose();
        parent.assLoader.manager.dispose();
    }
}