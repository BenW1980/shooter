package game.peanutpanda.shooter.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

import game.peanutpanda.shooter.Shooter;
import game.peanutpanda.shooter.states.ScreenSize;
import game.peanutpanda.shooter.states.ScreenState;
import game.peanutpanda.shooter.ui.Button;


public class CreditsScreen extends GameScreen {


    private Button backButton;
    private Button twitterButton;

    public CreditsScreen(Shooter parent) {
        super(parent);
        Gdx.input.setCatchBackKey(true);
        this.initGrass(MathUtils.random(1, 3));
        this.randomiseBackground();
        int btnSize = 90;

        this.backButton = new Button(controller, viewport, batch,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 50, ScreenSize.HEIGHT.getSize() / 2 - 258, btnSize, btnSize),
                assetLoader.prevUnclicked(), assetLoader.prevClicked());

        this.twitterButton = new Button(controller, viewport, batch,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 100, ScreenSize.HEIGHT.getSize() / 2 - 133, btnSize - 20, btnSize - 20),
                assetLoader.twitterUnclicked(), assetLoader.twitterClicked());

    }

    @Override
    public void render(float delta) {
        super.render(delta);
        batch.begin();

        for (Sprite sprite : grassSprites) {
            sprite.draw(batch);
        }

        batch.draw(assetLoader.creditsBackground(), 5, 182, 470, 616);

        backButton.draw(audioManager);
        twitterButton.draw(audioManager);

        batch.end();

        if (backButton.isProcessed() || Gdx.input.isKeyPressed(Input.Keys.BACK)) {
            parent.changeScreen(ScreenState.OPTIONS);
        }

        if (twitterButton.isProcessed()) {
            if (!Gdx.net.openURI("twitter://user?user_id=741271262006251520")) { // open app
                Gdx.net.openURI("https://twitter.com/PeanutPandaApps"); // open site
            }
            twitterButton.setProcessed(false);
        }

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
