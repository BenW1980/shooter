package game.peanutpanda.shooter.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

import game.peanutpanda.shooter.Shooter;
import game.peanutpanda.shooter.states.ScreenSize;
import game.peanutpanda.shooter.states.ScreenState;
import game.peanutpanda.shooter.ui.Button;
import game.peanutpanda.shooter.ui.ToggleButton;


public class OptionsScreen extends GameScreen {


    private Button mainMenuButton, infoButton;
    private ToggleButton musicBtn;
    private ToggleButton soundBtn;
    private ToggleButton shakeBtn;
    private ToggleButton exploBtn;

    public OptionsScreen(Shooter parent) {
        super(parent);
        Gdx.input.setCatchBackKey(true);
        this.initGrass(MathUtils.random(1, 3));
        this.randomiseBackground();
        int btnSize = 90;
        int tglSize = 50;

        this.mainMenuButton = new Button(controller, viewport, batch,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 50, ScreenSize.HEIGHT.getSize() / 2 - 230, btnSize, btnSize),
                assetLoader.homeUnclicked(), assetLoader.homeClicked());

        this.infoButton = new Button(controller, viewport, batch,
                new Rectangle(ScreenSize.WIDTH.getSize() - 80, 20, 60, 60),
                assetLoader.infoUnclicked(), assetLoader.infoClicked());

        this.musicBtn = new ToggleButton(controller, viewport, batch,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 120, ScreenSize.HEIGHT.getSize() / 2 + 208, tglSize, tglSize),
                assetLoader.emptyGreenUnclicked(), assetLoader.emptyGreenClicked(), assetLoader.yesUnclicked(), assetLoader.yesClicked());
        this.musicBtn.setToggled(gameData.musicIsOn());

        this.soundBtn = new ToggleButton(controller, viewport, batch,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 120, ScreenSize.HEIGHT.getSize() / 2 + 112, tglSize, tglSize),
                assetLoader.emptyGreenUnclicked(), assetLoader.emptyGreenClicked(), assetLoader.yesUnclicked(), assetLoader.yesClicked());
        this.soundBtn.setToggled(gameData.soundIsOn());

        this.shakeBtn = new ToggleButton(controller, viewport, batch,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 120, ScreenSize.HEIGHT.getSize() / 2 + 17, tglSize, tglSize),
                assetLoader.emptyGreenUnclicked(), assetLoader.emptyGreenClicked(), assetLoader.yesUnclicked(), assetLoader.yesClicked());
        this.shakeBtn.setToggled(gameData.screenShakeIsOn());

        this.exploBtn = new ToggleButton(controller, viewport, batch,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 120, ScreenSize.HEIGHT.getSize() / 2 - 80, tglSize, tglSize),
                assetLoader.emptyGreenUnclicked(), assetLoader.emptyGreenClicked(), assetLoader.yesUnclicked(), assetLoader.yesClicked());
        this.exploBtn.setToggled(gameData.hugeExplosionsAreOn());
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        batch.begin();

        for (Sprite sprite : grassSprites) {
            sprite.draw(batch);
        }

        batch.draw(assetLoader.optionsBackground(), 5, 210, 470, 588);

        fontNobileMedium.draw(batch, "Music",
                60, ScreenSize.HEIGHT.getSize() / 2 + 240);
        fontNobileMedium.draw(batch, "Sound",
                60, ScreenSize.HEIGHT.getSize() / 2 + 145);
        fontNobileMedium.draw(batch, "Screenshake",
                60, ScreenSize.HEIGHT.getSize() / 2 + 45);
        fontNobileMedium.draw(batch, "Huge Explosions",
                60, ScreenSize.HEIGHT.getSize() / 2 - 45);

        mainMenuButton.draw(audioManager);
        infoButton.draw(audioManager);

        musicBtn.draw(audioManager);
        soundBtn.draw(audioManager);
        shakeBtn.draw(audioManager);
        exploBtn.draw(audioManager);

        batch.end();

        if (mainMenuButton.isProcessed() || Gdx.input.isKeyPressed(Input.Keys.BACK)) {
            parent.changeScreen(ScreenState.MAIN_MENU);
        }

        if (infoButton.isProcessed()) {
            parent.changeScreen(ScreenState.CREDITS);
        }

        gameData.saveScreenShakePref(shakeBtn.isToggled());
        gameData.saveExplosionSizePref(exploBtn.isToggled());
        gameData.saveMusicPref(musicBtn.isToggled());
        gameData.saveSoundPref(soundBtn.isToggled());
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
