package game.peanutpanda.shooter.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

import java.io.File;

import game.peanutpanda.shooter.Level;
import game.peanutpanda.shooter.Shooter;
import game.peanutpanda.shooter.factories.BlockFactory;
import game.peanutpanda.shooter.factories.EnemyFactory;
import game.peanutpanda.shooter.gameobjects.Block;
import game.peanutpanda.shooter.gameobjects.Enemy;
import game.peanutpanda.shooter.gameobjects.Player;
import game.peanutpanda.shooter.gameobjects.Projectile;
import game.peanutpanda.shooter.states.BattleState;
import game.peanutpanda.shooter.states.GameState;
import game.peanutpanda.shooter.states.ScreenSize;
import game.peanutpanda.shooter.states.ScreenState;
import game.peanutpanda.shooter.ui.ConfirmQuitWindow;
import game.peanutpanda.shooter.ui.GameOverWindow;
import game.peanutpanda.shooter.ui.HUD;
import game.peanutpanda.shooter.ui.NextLevelWindow;


public class BattleScreen extends GameScreen {

    private Player player;

    private ShaderProgram flashShader = new ShaderProgram(Gdx.files.internal(
            "shaders" + File.separator + "flash-vert.glsl").readString(),
            Gdx.files.internal("shaders" + File.separator + "flash-frag.glsl").readString());

    private ShaderProgram defaultShader = SpriteBatch.createDefaultShader();

    private TiledMap tiledMap;
    private TiledMapRenderer tiledMapRenderer;

    private Array<Enemy> enemies;
    private float enemyShaderTime;
    private float blockShaderTime;
    private float timeSpentInLevel;
    private EnemyFactory enemyFactory;
    private BlockFactory blockFactory;
    private Array<Block> blocks;
    private Array<Rectangle> walls;

    private Array<MapObjects> tiles;
    private Array<MapObjects> wallArray;
    private Array<MapObjects> blockWallArray;
    private Array<Rectangle> blockWalls;

    private MapObject floorMapObject;
    private Rectangle floor;

    private long lastSpawn;
    private Rectangle explosionRect;

    private Level level;
    private HUD hud;
    private NextLevelWindow nextLevelWindow;
    private GameOverWindow gameOverWindow;
    private ConfirmQuitWindow confirmQuitWindow;

    public BattleScreen(Shooter parent) {
        super(parent);
        Gdx.input.setCatchBackKey(true);
        this.level = new Level();
        player = new Player(batch, assetLoader);
        this.hud = new HUD(player, assetLoader, batch, fontNobile);
        this.init();
    }

    private void init() {
        this.gameState = GameState.IN_BATTLE;
        level.setEnemySpawnTime(999999999);
        timeSpentInLevel = 0;
        level.incrementCounter();
        player.setEnemiesKilled(0);

        if (level.getCount() % 7 == 0) {
            battleState = BattleState.SPACE_BATTLE;
            player.upgradeFirepower();
        } else {
            battleState = BattleState.LAND_BATTLE;
        }

        this.enemies = new Array<Enemy>();
        this.enemyFactory = new EnemyFactory(assetLoader);
        this.blockFactory = new BlockFactory(assetLoader);
        this.blocks = new Array<Block>();
        this.tiles = new Array<MapObjects>();
        this.wallArray = new Array<MapObjects>();
        this.walls = new Array<Rectangle>();
        this.blockWallArray = new Array<MapObjects>();
        this.blockWalls = new Array<Rectangle>();

        if (battleState == BattleState.SPACE_BATTLE) {
            this.tiledMap = parent.assLoader.manager.get("maps/space.tmx");
        } else if (battleState == BattleState.LAND_BATTLE) {
            this.tiledMap = parent.assLoader.manager.get("maps/map" + MathUtils.random(1, 30) + ".tmx");
            this.tiles.addAll(tiledMap.getLayers().get("tiles").getObjects());
            this.createTiles(tiles);
            this.wallArray.addAll(tiledMap.getLayers().get("walls").getObjects());
            this.createWalls(wallArray);
            this.blockWallArray.addAll(tiledMap.getLayers().get("blockWalls").getObjects());
            this.createBlockWalls(blockWallArray);
            this.floorMapObject = tiledMap.getLayers().get("floor").getObjects().get(0);
            this.floor = new Rectangle(((RectangleMapObject) floorMapObject).getRectangle());
        }

        this.tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap);

        this.initGrass(MathUtils.random(1, 3));

        this.randomiseBackground();

        this.explosionRect = new Rectangle();

        this.nextLevelWindow = new NextLevelWindow(assetLoader, batch, controller, viewport, player, fontWolf);
        this.gameOverWindow = new GameOverWindow(assetLoader, batch, controller, viewport, player, fontWolf, audioManager);
        this.confirmQuitWindow = new ConfirmQuitWindow(assetLoader, batch, controller, viewport, audioManager);
        //levelupbar
        if (level.getCount() != 7) {
            this.nextLevelWindow.getLevelUpBar().setCurrentBarWidth(nextLevelWindow.getLevelUpBar().PART_BAR * (level.getCount() - 1));
        }
        this.level.initNextLevel(blocks);
        player.setTimesHeartAdded(0);
    }

    private void createEnemies() {

        long spawnTime;

        if (battleState == BattleState.LAND_BATTLE) {
            spawnTime = level.getEnemySpawnTime();
        } else {
            spawnTime = 250000000;
        }

        if (TimeUtils.nanoTime() - lastSpawn > spawnTime) {
            if (battleState == BattleState.LAND_BATTLE) {
                if (level.getRealCount() <= 5) {
                    spawnThree();
                } else if (level.getRealCount() > 5 && level.getRealCount() <= 18) {
                    spawnFour();
                } else if (level.getRealCount() > 18) {
                    spawnFive();
                }

            } else if (battleState == BattleState.SPACE_BATTLE) {
                enemies.add(enemyFactory.createSpaceEnemy(level.getSpaceEnemyType(), level.getRealCount()));
            }
            lastSpawn = TimeUtils.nanoTime();
        }
    }

    private void spawnThree() {
        enemies.add(enemyFactory.create(level.getEnemyType1(), ScreenSize.WIDTH.getSize() / 2 - 200, level.getRealCount()));
        enemies.add(enemyFactory.create(level.getEnemyType2(), ScreenSize.WIDTH.getSize() / 2 - 25, level.getRealCount()));
        enemies.add(enemyFactory.create(level.getEnemyType3(), ScreenSize.WIDTH.getSize() / 2 + 150, level.getRealCount()));
    }

    private void spawnFour() {
        enemies.add(enemyFactory.create(level.getEnemyType1(), ScreenSize.WIDTH.getSize() / 2 - 200, level.getRealCount()));
        enemies.add(enemyFactory.create(level.getEnemyType2(), ScreenSize.WIDTH.getSize() / 2 - 75, level.getRealCount()));
        enemies.add(enemyFactory.create(level.getEnemyType3(), ScreenSize.WIDTH.getSize() / 2 + 25, level.getRealCount()));
        enemies.add(enemyFactory.create(level.getEnemyType4(), ScreenSize.WIDTH.getSize() / 2 + 150, level.getRealCount()));
    }

    private void spawnFive() {
        enemies.add(enemyFactory.create(level.getEnemyType1(), ScreenSize.WIDTH.getSize() / 2 - 200, level.getRealCount()));
        enemies.add(enemyFactory.create(level.getEnemyType2(), ScreenSize.WIDTH.getSize() / 2 - 100, level.getRealCount()));
        enemies.add(enemyFactory.create(level.getEnemyType3(), ScreenSize.WIDTH.getSize() / 2 - 25, level.getRealCount()));
        enemies.add(enemyFactory.create(level.getEnemyType4(), ScreenSize.WIDTH.getSize() / 2 + 50, level.getRealCount()));
        enemies.add(enemyFactory.create(level.getEnemyType5(), ScreenSize.WIDTH.getSize() / 2 + 125, level.getRealCount()));
    }

    @Override
    public void show() {
    }

    private Array<Vector2> getPolygonVertices(Polygon polygon) {
        float[] vertices = polygon.getTransformedVertices();

        Array<Vector2> result = new Array<Vector2>();
        for (int i = 0; i < vertices.length / 2; i++) {
            float x = vertices[i * 2];
            float y = vertices[i * 2 + 1];
            result.add(new Vector2(x, y));
        }
        return result;
    }

    private void createTiles(Array<MapObjects> mapObjects) {
        int random = MathUtils.random(1, 7);
        for (MapObjects obj1 : mapObjects) {
            for (MapObject obj2 : obj1) {
                blocks.add(blockFactory.createRandom(obj2, random, level.getRealCount()));
            }
        }
    }

    private void createWalls(Array<MapObjects> mapObjects) {
        for (MapObjects collObjects : mapObjects) {
            for (MapObject collObject : collObjects) {
                walls.add(new Rectangle(((RectangleMapObject) collObject).getRectangle()));
            }
        }
    }

    private void createBlockWalls(Array<MapObjects> mapObjects) {
        for (MapObjects collObjects : mapObjects) {
            for (MapObject collObject : collObjects) {
                blockWalls.add(new Rectangle(((RectangleMapObject) collObject).getRectangle()));
            }
        }
    }

    private void finishLevel() {

        switch (battleState) {
            case LAND_BATTLE:
                if (blocks.size == 0) {
                    gameState = GameState.NEXT_LEVEL_MENU;
                }
                break;
            case SPACE_BATTLE:
                if (player.getEnemiesKilled() == 50) {
                    gameState = GameState.NEXT_LEVEL_MENU;
                }
                break;
        }
    }

    private void gameOver() {
        if (player.getHeartArray().size < 1 && player.isHeartsFilledAfterStart()) {
            audioManager.playDeath();
            gameState = GameState.GAME_OVER;
        }
    }

    @Override
    public void render(float delta) {

        super.render(delta);

        tiledMapRenderer.setView(camera);
        tiledMapRenderer.render();

        Array<Vector2> turretPolyArray = getPolygonVertices(player.getTurretPoly());

        float accumulator = 0;
        float frameTime = Math.min(delta, 1 / 60f);
        accumulator += frameTime;
        while (accumulator >= 0.1) {
            accumulator -= 0.1;
        }

        if (enemies.size < 100) {
            createEnemies();
        }

        if (!player.isHeartsFilledAfterStart()) {
            player.fillHeartsAfterStart(assetLoader);
        }

        batch.begin();

        switch (gameState) {

            case IN_BATTLE:

                if (Gdx.input.isKeyPressed(Input.Keys.BACK)) {
                    gameState = GameState.PAUSED;
                }

                confirmQuitWindow.getNoButton().setProcessed(false);

                finishLevel();
                gameOver();

                timeSpentInLevel += delta;

                if (battleState == BattleState.SPACE_BATTLE) {
                    batch.draw(spaceBackground, 0, 0, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
                }

                player.update(audioManager, viewport, controller, gameState, turretPolyArray);

                for (Projectile projectile : player.getProjectiles()) {
                    projectile.updateProjectilesForPlayer(player, batch, accumulator, blocks, enemies);
                }

                batch.end();

                shaderBatch.begin();

                for (Enemy enemy : enemies) {

                    if (explosionRect.overlaps(enemy.getRectangle())) {
                        enemy.setSecondHandNuked(true);
                        enemy.setDead(true);
                    }
                }

                if (explosionRect.width > 10) {
                    explosionRect = new Rectangle();
                }

                for (Enemy enemy : enemies) {

                    if (battleState == BattleState.LAND_BATTLE && timeSpentInLevel > 20.0f) {
                        level.setEnemySpawnTime(399999999);
                        enemy.setSpeed(3.5f);
                    }

                    if (enemy.isNuked()) {
                        explosionRect = enemy.getNukeRect();
                    }

                    if (enemy.isHit()) {
                        if (battleState == BattleState.SPACE_BATTLE) {
                            enemy.getRectangle().setY(enemy.getRectangle().y + 2);
                        }
                        enemyShaderTime += accumulator;
                        if (enemyShaderTime < 0.05f) {
                            shaderBatch.setShader(flashShader);

                        } else {
                            enemyShaderTime = 0;
                            enemy.setHit(false);
                        }

                    } else {
                        shaderBatch.setShader(defaultShader);
                    }

                    if (enemy.isDead()) {
                        shaderBatch.setShader(defaultShader);
                    }

                    enemy.update(shaderBatch, player, gameState, battleState, assetLoader, accumulator,
                            screenShake, enemies, blocks, walls, blockWalls, floor, gameData, audioManager);
                }

                for (Block block : blocks) {


                    if (block.isHit()) {
                        block.getRectangle().set(block.getRectangle().x, block.getRectangle().y + 0.8f, block.getRectangle().width + 1.5f, block.getRectangle().height + 1.5f);
                        blockShaderTime += accumulator;
                        if (blockShaderTime < 0.05f) {
                            shaderBatch.setShader(flashShader);

                        } else {
                            block.getRectangle().set(block.getOriginalX(), block.getOriginalY(), block.getOriginalWidth(), block.getOriginalHeight());
                            blockShaderTime = 0;
                            block.setHit(false);
                        }

                    } else {
                        shaderBatch.setShader(defaultShader);
                    }

                    if (block.isDead()) {
                        shaderBatch.setShader(defaultShader);
                    }

                    block.update(audioManager, shaderBatch, player, blocks, screenShake, delta, gameData);
                }

                shaderBatch.end();

                batch.begin();

                for (Enemy enemy : enemies) {
                    enemy.drawScore(delta, fontNobile, batch, 75, gameState);
                }

                for (Block block : blocks) {
                    block.drawScore(delta, fontNobile, batch, 100, gameState);
                }

                break;

            case NEXT_LEVEL_MENU:
                confirmQuitWindow.getNoButton().setProcessed(false);
                nextLevelWindow.draw(level.getCount(), gameData, audioManager);
                if (nextLevelWindow.getNextLevelButton().isProcessed()) {
                    init();
                }
                if (nextLevelWindow.getMainMenuButton().isProcessed()) {
                    gameState = GameState.CONFIRM_QUIT;
                }
                break;

            case CONFIRM_QUIT:
                nextLevelWindow.draw(level.getCount(), gameData, audioManager);
                nextLevelWindow.getMainMenuButton().setProcessed(false);
                confirmQuitWindow.draw();
                if (confirmQuitWindow.getYesButton().isProcessed()) {
                    parent.changeScreen(ScreenState.MAIN_MENU);
                }
                if (confirmQuitWindow.getNoButton().isProcessed()) {
                    gameState = GameState.NEXT_LEVEL_MENU;
                }
                break;

            case GAME_OVER:
                this.gameOverWindow.draw(gameData);
                if (gameOverWindow.getMainMenuButton().isProcessed()) {
                    parent.changeScreen(ScreenState.MAIN_MENU);
                }
                break;

            case PAUSED:
                confirmQuitWindow.draw();
                if (confirmQuitWindow.getYesButton().isProcessed()) {
                    gameData.saveHighscore(player.getScore());
                    parent.changeScreen(ScreenState.MAIN_MENU);
                }
                if (confirmQuitWindow.getNoButton().isProcessed()) {
                    gameState = GameState.IN_BATTLE;
                }
                break;
        }

        if (battleState != BattleState.SPACE_BATTLE) {
            for (Sprite sprite : grassSprites) {
                sprite.draw(batch);
            }
        }

        hud.draw(blocks, battleState);

        batch.end();

    }


    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
        tiledMap.dispose();
        flashShader.dispose();
    }
}