package game.peanutpanda.shooter.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

import game.peanutpanda.shooter.Shooter;
import game.peanutpanda.shooter.gameobjects.Enemy;
import game.peanutpanda.shooter.states.ScreenSize;
import game.peanutpanda.shooter.states.ScreenState;
import game.peanutpanda.shooter.ui.Button;


public class TutorialScreen extends GameScreen {

    private Button startButton, backButton;
    private Array<Enemy> enemies;
    private float elapsedTime;

    public TutorialScreen(Shooter parent) {
        super(parent);
        Gdx.input.setCatchBackKey(true);
        this.initGrass(MathUtils.random(1, 3));
        this.randomiseBackground();
        int btnSize = 90;

        this.backButton = new Button(controller, viewport, batch,
                new Rectangle(ScreenSize.WIDTH.getSize() - 80, 20, 60, 60),
                assetLoader.prevUnclicked(), assetLoader.prevClicked());

        this.startButton = new Button(controller, viewport, batch,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 50, ScreenSize.HEIGHT.getSize() / 2 - 285, btnSize + 20, btnSize + 20),
                assetLoader.nextUnclicked(), assetLoader.nextClicked());

        enemies = new Array<Enemy>();

        enemies.add(new Enemy(assetLoader, "animations/enemies/monster.atlas", 1f / 200f,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 190, ScreenSize.HEIGHT.getSize() / 2 - 140, 60, 60)));

        enemies.add(new Enemy(assetLoader, "animations/enemies/green.atlas", 1f / 20f,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 110, ScreenSize.HEIGHT.getSize() / 2 - 140, 60, 60)));

        enemies.add(new Enemy(assetLoader, "animations/enemies/orange.atlas", 1f / 200f,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 30, ScreenSize.HEIGHT.getSize() / 2 - 140, 60, 60)));

        enemies.add(new Enemy(assetLoader, "animations/enemies/horny.atlas", 1f / 150f,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 50, ScreenSize.HEIGHT.getSize() / 2 - 140, 60, 60)));

        enemies.add(new Enemy(assetLoader, "animations/enemies/brown.atlas", 1f / 30f,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 130, ScreenSize.HEIGHT.getSize() / 2 - 140, 60, 60)));
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        elapsedTime += delta;

        batch.begin();

        batch.draw(assetLoader.tutBackground(), 5, 170, 470, 626);

        batch.draw(assetLoader.allBlocks(), ScreenSize.WIDTH.getSize() / 2 - 210, ScreenSize.HEIGHT.getSize() / 2 + 80, 421, 60);

        for (Sprite sprite : grassSprites) {
            sprite.draw(batch);
        }

        startButton.draw(audioManager);
        backButton.draw(audioManager);

        for (Enemy enemy : enemies) {
            enemy.getSprite().setRegion(enemy.getMoveAnimation().getKeyFrame(elapsedTime, true));
            batch.draw(enemy.getSprite(), enemy.getRectangle().x, enemy.getRectangle().y, enemy.getRectangle().width, enemy.getRectangle().height);
        }

        batch.end();

        if (startButton.isProcessed()) {
            parent.changeScreen(ScreenState.IN_GAME);
        }

        if (backButton.isProcessed() || Gdx.input.isKeyPressed(Input.Keys.BACK)) {
            parent.changeScreen(ScreenState.MAIN_MENU);
        }

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
