package game.peanutpanda.shooter.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

import game.peanutpanda.shooter.Shooter;
import game.peanutpanda.shooter.states.ScreenSize;
import game.peanutpanda.shooter.states.ScreenState;
import game.peanutpanda.shooter.ui.Button;


public class HighScoreScreen extends GameScreen {

    private Button mainMenuButton;
    private Sprite rays;
    private float timePassed;


    public HighScoreScreen(Shooter parent) {
        super(parent);
        Gdx.input.setCatchBackKey(true);
        this.initGrass(MathUtils.random(1, 3));
        this.randomiseBackground();
        this.rays = new Sprite(assetLoader.rays());
        int btnSize = 90;

        this.mainMenuButton = new Button(controller, viewport, batch,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 50, ScreenSize.HEIGHT.getSize() / 2 - 230, btnSize, btnSize),
                assetLoader.homeUnclicked(), assetLoader.homeClicked());
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        batch.begin();

        timePassed += Gdx.graphics.getDeltaTime();
        rays.setRotation(timePassed * 100);
        rays.setPosition(ScreenSize.WIDTH.getSize() / 2 - (rays.getWidth() / 2), ScreenSize.HEIGHT.getSize() / 2 + 280 - (rays.getHeight() / 2));

        this.rays.setOriginCenter();
        rays.draw(batch);

        for (Sprite sprite : grassSprites) {
            sprite.draw(batch);
        }

        int hiScore = gameData.highScore();
        String hiString = Integer.toString(hiScore);

        batch.draw(assetLoader.highscoreBackground(), 5, 210, 470, 588);
        fontNobileLarge.draw(batch, "" + String.format("%,d", gameData.highScore()),
                ScreenSize.WIDTH.getSize() / 2 - (hiString.length() * 18), ScreenSize.HEIGHT.getSize() / 2 + 100);

        fontNobile.draw(batch, "Rank : " + rank(hiScore),
                50, ScreenSize.HEIGHT.getSize() / 2 - 30);

        mainMenuButton.draw(audioManager);

        batch.end();

        if (mainMenuButton.isProcessed() || Gdx.input.isKeyPressed(Input.Keys.BACK)) {
            parent.changeScreen(ScreenState.MAIN_MENU);
        }

    }

    private String rank(int score) {

        if (score > 0 && score < 10000) {
            return "Cheeseball";
        } else if (score >= 10000 && score < 20000) {
            return "Lobster King";
        } else if (score >= 20000 && score < 40000) {
            return "Pretender";
        } else if (score >= 40000 && score < 60000) {
            return "Low Roller";
        } else if (score >= 60000 && score < 80000) {
            return "Crabcakes";
        } else if (score >= 80000 && score < 100000) {
            return "Fishy";
        } else if (score >= 100000 && score < 120000) {
            return "Steak Dinner";
        } else if (score >= 120000 && score < 140000) {
            return "Potatoes";
        } else if (score >= 140000 && score < 160000) {
            return "Al Dente";
        } else if (score >= 160000 && score < 180000) {
            return "Clown";
        } else if (score >= 180000 && score < 200000) {
            return "Paladin";
        } else if (score >= 200000 && score < 220000) {
            return "Anomaly";
        } else if (score >= 220000 && score < 240000) {
            return "Wizard";
        } else if (score >= 240000 && score < 260000) {
            return "Protector";
        } else if (score >= 260000 && score < 280000) {
            return "Mountaineer";
        } else if (score > 280000 && score < 300000) {
            return "Strongific";
        } else if (score > 300000 && score < 320000) {
            return "Big Betty";
        } else if (score > 320000 && score < 340000) {
            return "Feverish";
        } else if (score > 340000 && score < 360000) {
            return "High Roller";
        } else if (score > 360000 && score < 380000) {
            return "Trooper";
        } else if (score > 380000 && score < 400000) {
            return "Rocketeer";
        } else if (score > 400000 && score < 420000) {
            return "Power Player";
        } else if (score > 420000 && score < 440000) {
            return "Beast Master";
        } else if (score > 440000 && score < 460000) {
            return "Fantabulous";
        } else if (score > 460000 && score < 480000) {
            return "Scary";
        } else if (score > 480000 && score < 500000) {
            return "Combo Breaker";
        } else if (score > 500000 && score < 550000) {
            return "Untouchable";
        } else if (score > 550000 && score < 600000) {
            return "Kingy";
        } else if (score > 600000 && score < 700000) {
            return "Queeny";
        } else if (score > 700000 && score < 800000) {
            return "Towering";
        } else if (score > 800000 && score < 900000) {
            return "Godly";
        } else if (score > 900000 && score < 1000000) {
            return "Sid Meier";
        } else if (score > 1000000) {
            return "Maximus!";
        }

        return "None";
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
