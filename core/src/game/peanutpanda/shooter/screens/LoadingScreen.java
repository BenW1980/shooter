package game.peanutpanda.shooter.screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;

import game.peanutpanda.shooter.Shooter;
import game.peanutpanda.shooter.states.ScreenSize;
import game.peanutpanda.shooter.states.ScreenState;

public class LoadingScreen implements Screen {

    private Sprite pandaSprite;
    private Sprite pandaTxtSprite;
    private Sprite loadingBar;
    private Sprite loadingBarBackground;

    private Shooter parent;
    private BitmapFont font;

    public LoadingScreen(Shooter parent) {
        this.parent = parent;
        this.pandaSprite = new Sprite(new Texture(Gdx.files.internal("loadingAssets/pepaWhite.png")));
        this.pandaTxtSprite = new Sprite(new Texture(Gdx.files.internal("loadingAssets/pepatxt.png")));
        this.loadingBar = new Sprite(new Texture(Gdx.files.internal("loadingAssets/loadingBar.png")));
        this.loadingBarBackground = new Sprite(new Texture(Gdx.files.internal("loadingAssets/loadingBarBackground.png")));
        this.font = new BitmapFont();
        this.font.setColor(Color.BLACK);
        this.font.getData().setScale(2.0f);
    }

    @Override
    public void show() {

        parent.assLoader.loadSounds();
        parent.assLoader.loadFontHandling();
        parent.assLoader.loadTiledMaps();
        parent.assLoader.loadMusic();
        parent.assLoader.loadAtlas();
        parent.assLoader.loadImages();

    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(255 / 255f, 255 / 255f, 255 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        parent.camera.update();
        parent.batch.setProjectionMatrix(parent.camera.combined);

        parent.batch.begin();

        pandaTxtSprite.setBounds(100, 365, 200, 113);
        pandaTxtSprite.draw(parent.batch);

        pandaSprite.setBounds(330, 365, 75, 113);
        pandaSprite.draw(parent.batch);

        loadingBar.setPosition(ScreenSize.WIDTH.getSize() / 2 - 150, ScreenSize.HEIGHT.getSize() / 2 - 200);
        loadingBar.setSize(parent.assLoader.getManager().getProgress() * 300, 46);
        loadingBarBackground.setPosition(ScreenSize.WIDTH.getSize() / 2 - 150, ScreenSize.HEIGHT.getSize() / 2 - 200);
        loadingBarBackground.setSize(300, 46);
        loadingBar.draw(parent.batch);
        loadingBarBackground.draw(parent.batch);

        parent.batch.end();

        if (parent.assLoader.getManager().update()) {

            pandaSprite.setAlpha(pandaSprite.getColor().a - delta * 1.5f);
            pandaTxtSprite.setAlpha(pandaTxtSprite.getColor().a - delta * 1.5f);

            if (pandaSprite.getColor().a < 0.1f) {
                pandaSprite.setAlpha(0);
                pandaTxtSprite.setAlpha(0);
                parent.changeScreen(ScreenState.MAIN_MENU);
            }
        }
    }

    @Override
    public void resize(int width, int height) {
        parent.viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
//        parent.batch.dispose();
//        pandaTexture.dispose();
//        pandaTxtTexture.dispose();
//        parent.assLoader.getManager().dispose();
    }


}
