package game.peanutpanda.shooter;


import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

import game.peanutpanda.shooter.factories.EnemyType;
import game.peanutpanda.shooter.gameobjects.Block;

public class Level {

    private EnemyType enemyType1;
    private EnemyType enemyType2;
    private EnemyType enemyType3;
    private EnemyType enemyType4;
    private EnemyType enemyType5;
    private EnemyType spaceEnemyType;
    private Array<EnemyType> enemyTypes;
    private Array<EnemyType> spaceEnemyTypes;
    private float count;
    private float realCount;
    private long enemySpawnTime;

    public Level() {
        enemySpawnTime = 999999999;
    }

    public void initNextLevel(Array<Block> blocks) {

        enemyTypes = new Array<EnemyType>();
        spaceEnemyTypes = new Array<EnemyType>();
        enemyTypes.addAll(EnemyType.BROWN, EnemyType.GREEN, EnemyType.GREY_SQUARE, EnemyType.HORNY, EnemyType.ORANGE);
        spaceEnemyTypes.addAll(EnemyType.ROCKET, EnemyType.BULLET);

        enemyType1 = enemyTypes.get(MathUtils.random(0, 4));
        enemyTypes.removeValue(enemyType1, true);
        enemyType2 = enemyTypes.get(MathUtils.random(0, 3));
        enemyTypes.removeValue(enemyType2, true);
        enemyType3 = enemyTypes.get(MathUtils.random(0, 2));
        enemyTypes.removeValue(enemyType3, true);
        enemyType4 = enemyTypes.get(MathUtils.random(0, 1));
        enemyTypes.removeValue(enemyType4, true);
        enemyType5 = enemyTypes.get(0);

        spaceEnemyType = spaceEnemyTypes.get(MathUtils.random(0, 1));

        for (Block block : blocks) {
            if (count % 3 == 0) {
                block.setHitPoints(block.getHitPoints() + 1);
            }
        }


    }

    public void incrementCounter() {
        count++;
        realCount++;
        if (count > 7) {
            count = 1;
        }
    }

    public EnemyType getEnemyType1() {
        return enemyType1;
    }

    public EnemyType getEnemyType2() {
        return enemyType2;
    }

    public EnemyType getEnemyType3() {
        return enemyType3;
    }

    public EnemyType getEnemyType4() {
        return enemyType4;
    }

    public EnemyType getEnemyType5() {
        return enemyType5;
    }

    public EnemyType getSpaceEnemyType() {
        return spaceEnemyType;
    }

    public float getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public float getRealCount() {
        return realCount;
    }

    public void setRealCount(float realCount) {
        this.realCount = realCount;
    }

    public long getEnemySpawnTime() {
        return enemySpawnTime;
    }

    public void setEnemySpawnTime(long enemySpawnTime) {
        this.enemySpawnTime = enemySpawnTime;
    }
}