package game.peanutpanda.shooter.assetmanager;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.utils.Array;

public class AssetLoader {

    public final AssetManager manager = new AssetManager();

    public void loadFontHandling() {

        FileHandleResolver resolver = new InternalFileHandleResolver();
        manager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(resolver));
        manager.setLoader(BitmapFont.class, ".ttf", new FreetypeFontLoader(resolver));

        FreetypeFontLoader.FreeTypeFontLoaderParameter fontNobile = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        fontNobile.fontFileName = "fonts/Nobile-Bold.ttf";
        fontNobile.fontParameters.size = 23;
        fontNobile.fontParameters.borderWidth = 2.8f;
        fontNobile.fontParameters.borderColor = Color.BLACK;
        fontNobile.fontParameters.color = Color.WHITE;
        manager.load("Nobile-Bold.ttf", BitmapFont.class, fontNobile);

        FreetypeFontLoader.FreeTypeFontLoaderParameter fontNobileMedium = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        fontNobileMedium.fontFileName = "fonts/Nobile-Bold.ttf";
        fontNobileMedium.fontParameters.size = 25;
        fontNobileMedium.fontParameters.borderWidth = 2.8f;
        fontNobileMedium.fontParameters.borderColor = Color.BLACK;
        fontNobileMedium.fontParameters.color = Color.WHITE;
        manager.load("Nobile-BoldMedium.ttf", BitmapFont.class, fontNobileMedium);

        FreetypeFontLoader.FreeTypeFontLoaderParameter fontNobileLarge = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        fontNobileLarge.fontFileName = "fonts/Nobile-Bold.ttf";
        fontNobileLarge.fontParameters.size = 50;
        fontNobileLarge.fontParameters.borderWidth = 2.8f;
        fontNobileLarge.fontParameters.borderColor = Color.BLACK;
        fontNobileLarge.fontParameters.color = Color.WHITE;
        manager.load("Nobile-BoldLarge.ttf", BitmapFont.class, fontNobileLarge);

        FreetypeFontLoader.FreeTypeFontLoaderParameter fontWolf = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        fontWolf.fontFileName = "fonts/wolfsbane2.ttf";
        fontWolf.fontParameters.size = 48;
        fontWolf.fontParameters.borderWidth = 1.5f;
        fontWolf.fontParameters.borderColor = Color.LIGHT_GRAY;
        fontWolf.fontParameters.color = new Color(255f / 255f, 255f / 255f, 255f / 255f, 1);
        manager.load("wolfsbane2.ttf", BitmapFont.class, fontWolf);
    }

    public void loadTiledMaps() {
        manager.setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));
        manager.load("maps/space.tmx", TiledMap.class);
        manager.load("maps/map1.tmx", TiledMap.class);
        manager.load("maps/map2.tmx", TiledMap.class);
        manager.load("maps/map3.tmx", TiledMap.class);
        manager.load("maps/map4.tmx", TiledMap.class);
        manager.load("maps/map5.tmx", TiledMap.class);
        manager.load("maps/map6.tmx", TiledMap.class);
        manager.load("maps/map7.tmx", TiledMap.class);
        manager.load("maps/map8.tmx", TiledMap.class);
        manager.load("maps/map9.tmx", TiledMap.class);
        manager.load("maps/map10.tmx", TiledMap.class);
        manager.load("maps/map11.tmx", TiledMap.class);
        manager.load("maps/map12.tmx", TiledMap.class);
        manager.load("maps/map13.tmx", TiledMap.class);
        manager.load("maps/map14.tmx", TiledMap.class);
        manager.load("maps/map15.tmx", TiledMap.class);
        manager.load("maps/map16.tmx", TiledMap.class);
        manager.load("maps/map17.tmx", TiledMap.class);
        manager.load("maps/map18.tmx", TiledMap.class);
        manager.load("maps/map19.tmx", TiledMap.class);
        manager.load("maps/map20.tmx", TiledMap.class);
        manager.load("maps/map21.tmx", TiledMap.class);
        manager.load("maps/map22.tmx", TiledMap.class);
        manager.load("maps/map23.tmx", TiledMap.class);
        manager.load("maps/map24.tmx", TiledMap.class);
        manager.load("maps/map25.tmx", TiledMap.class);
        manager.load("maps/map26.tmx", TiledMap.class);
        manager.load("maps/map27.tmx", TiledMap.class);
        manager.load("maps/map28.tmx", TiledMap.class);
        manager.load("maps/map29.tmx", TiledMap.class);
        manager.load("maps/map30.tmx", TiledMap.class);
    }

    public void loadAtlas() {
        manager.load("animations/enemies/monster.atlas", TextureAtlas.class);
        manager.load("animations/enemies/horny.atlas", TextureAtlas.class);
        manager.load("animations/enemies/orange.atlas", TextureAtlas.class);
        manager.load("animations/enemies/brown.atlas", TextureAtlas.class);
        manager.load("animations/enemies/green.atlas", TextureAtlas.class);
        manager.load("animations/enemies/bullet.atlas", TextureAtlas.class);
        manager.load("animations/enemies/rocket.atlas", TextureAtlas.class);

        manager.load("animations/explosions/explo.atlas", TextureAtlas.class);
        manager.load("animations/explosions/exploBlock.atlas", TextureAtlas.class);
        manager.load("animations/explosions/nuke.atlas", TextureAtlas.class);
        manager.load("animations/powerups/powerup.atlas", TextureAtlas.class);
        manager.load("animations/powerups/powerupTaken.atlas", TextureAtlas.class);
    }

    public void loadImages() {
        loadBackgrounds();
        manager.load("ui/heart.png", Texture.class);
        manager.load("ui/emptyHeart.png", Texture.class);
        manager.load("ui/blackScreen.png", Texture.class);
        manager.load("ui/nextLevelMenuBackground.png", Texture.class);
        manager.load("ui/highScoreBackground.png", Texture.class);
        manager.load("ui/optionsBackground.png", Texture.class);
        manager.load("ui/creditsBackground.png", Texture.class);
        manager.load("ui/gameOver.png", Texture.class);
        manager.load("ui/hudBoard.png", Texture.class);
        manager.load("ui/rays.png", Texture.class);
        manager.load("ui/raysRed.png", Texture.class);
        manager.load("ui/progressBarContainer.png", Texture.class);
        manager.load("ui/progressBarRed.png", Texture.class);
        manager.load("ui/progressBarBlue.png", Texture.class);
        manager.load("ui/progressBarDark.png", Texture.class);
        manager.load("ui/title.png", Texture.class);
        manager.load("ui/star.png", Texture.class);
        manager.load("ui/quit.png", Texture.class);
        manager.load("ui/tutBackground.png", Texture.class);

        manager.load("ui/infoUnclicked.png", Texture.class);
        manager.load("ui/infoClicked.png", Texture.class);
        manager.load("ui/emptyGreenUnclicked.png", Texture.class);
        manager.load("ui/emptyGreenClicked.png", Texture.class);
        manager.load("ui/yesUnclicked.png", Texture.class);
        manager.load("ui/yesClicked.png", Texture.class);
        manager.load("ui/noUnclicked.png", Texture.class);
        manager.load("ui/noClicked.png", Texture.class);
        manager.load("ui/nextUnclicked.png", Texture.class);
        manager.load("ui/nextClicked.png", Texture.class);
        manager.load("ui/prevUnclicked.png", Texture.class);
        manager.load("ui/prevClicked.png", Texture.class);
        manager.load("ui/homeUnclicked.png", Texture.class);
        manager.load("ui/homeClicked.png", Texture.class);
        manager.load("ui/optionsUnclicked.png", Texture.class);
        manager.load("ui/optionsClicked.png", Texture.class);
        manager.load("ui/highScoreUnclicked.png", Texture.class);
        manager.load("ui/highScoreClicked.png", Texture.class);
        manager.load("ui/twitterUnclicked.png", Texture.class);
        manager.load("ui/twitterClicked.png", Texture.class);

        manager.load("images/fireball.png", Texture.class);
        manager.load("images/fireballRed.png", Texture.class);
        manager.load("images/fireballGreen.png", Texture.class);

        manager.load("images/redOverlay.png", Texture.class);

        manager.load("images/blocks/brown_rock.png", Texture.class);
        manager.load("images/blocks/green_rock.png", Texture.class);
        manager.load("images/blocks/purple_rock.png", Texture.class);
        manager.load("images/blocks/red_rock.png", Texture.class);
        manager.load("images/blocks/yellow_rock.png", Texture.class);
        manager.load("images/blocks/orange_rock.png", Texture.class);
        manager.load("images/blocks/blue_rock.png", Texture.class);
        manager.load("images/blocks/allBlocks.png", Texture.class);

        manager.load("images/turretBase.png", Texture.class);
        manager.load("images/turret1.png", Texture.class);

        manager.load("images/m1.png", Texture.class);
        manager.load("images/m2.png", Texture.class);
        manager.load("images/m5.png", Texture.class);
        manager.load("images/m6.png", Texture.class);
        manager.load("images/m7.png", Texture.class);

        manager.load("images/grass1.png", Texture.class);
        manager.load("images/grass2.png", Texture.class);
        manager.load("images/grass3.png", Texture.class);
        manager.load("images/grassBlue1.png", Texture.class);
        manager.load("images/grassBlue2.png", Texture.class);
        manager.load("images/grassBlue3.png", Texture.class);
        manager.load("images/grassPink1.png", Texture.class);
        manager.load("images/grassPink2.png", Texture.class);
        manager.load("images/grassPink3.png", Texture.class);

    }

    public void loadBackgrounds() {

        manager.load("images/backgrounds/meadow1.png", Texture.class);
        manager.load("images/backgrounds/meadow2.png", Texture.class);
        manager.load("images/backgrounds/meadow3.png", Texture.class);
        manager.load("images/backgrounds/meadow4.png", Texture.class);
        manager.load("images/backgrounds/meadow5.png", Texture.class);
        manager.load("images/backgrounds/mountain1.png", Texture.class);
        manager.load("images/backgrounds/mountain2.png", Texture.class);
        manager.load("images/backgrounds/mountain3.png", Texture.class);
        manager.load("images/backgrounds/cozy1.png", Texture.class);
        manager.load("images/backgrounds/cozy2.png", Texture.class);
        manager.load("images/backgrounds/cozy3.png", Texture.class);

        manager.load("images/backgrounds/space1.png", Texture.class);
        manager.load("images/backgrounds/space2.png", Texture.class);
        manager.load("images/backgrounds/space3.png", Texture.class);
        manager.load("images/backgrounds/space4.png", Texture.class);
    }

    public Array<Texture> spaceBackgroundArray() {

        Array<Texture> result = new Array<Texture>();

        Texture m1 = manager.get("images/backgrounds/space1.png", Texture.class);
        Texture m2 = manager.get("images/backgrounds/space2.png", Texture.class);
        Texture m3 = manager.get("images/backgrounds/space3.png", Texture.class);
        Texture m4 = manager.get("images/backgrounds/space4.png", Texture.class);

        result.addAll(m1, m2, m3, m4);

        return result;
    }

    public Array<Texture> backgroundArray() {

        Array<Texture> result = new Array<Texture>();

        Texture m1 = manager.get("images/backgrounds/meadow1.png", Texture.class);
        Texture m2 = manager.get("images/backgrounds/meadow2.png", Texture.class);
        Texture m3 = manager.get("images/backgrounds/meadow3.png", Texture.class);
        Texture m4 = manager.get("images/backgrounds/meadow4.png", Texture.class);
        Texture m5 = manager.get("images/backgrounds/meadow5.png", Texture.class);
        Texture m6 = manager.get("images/backgrounds/mountain1.png", Texture.class);
        Texture m7 = manager.get("images/backgrounds/mountain2.png", Texture.class);
        Texture m8 = manager.get("images/backgrounds/mountain3.png", Texture.class);
        Texture m9 = manager.get("images/backgrounds/cozy1.png", Texture.class);
        Texture m10 = manager.get("images/backgrounds/cozy2.png", Texture.class);
        Texture m11 = manager.get("images/backgrounds/cozy3.png", Texture.class);

        result.addAll(m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11);

        return result;
    }

    public Array<Texture> flashArray() {

        Array<Texture> result = new Array<Texture>();

        Texture m1 = manager.get("images/m1.png", Texture.class);
        Texture m2 = manager.get("images/m2.png", Texture.class);
        Texture m5 = manager.get("images/m5.png", Texture.class);
        Texture m6 = manager.get("images/m6.png", Texture.class);
        Texture m7 = manager.get("images/m7.png", Texture.class);

        result.addAll(m1, m2, m5, m6, m7);

        return result;
    }

    public void loadSounds() {
        manager.load("sounds/plop1.ogg", Sound.class);
        manager.load("sounds/boom.ogg", Sound.class);
        manager.load("sounds/death.ogg", Sound.class);
        manager.load("sounds/jingle1.ogg", Sound.class);
        manager.load("sounds/ping.ogg", Sound.class);
        manager.load("sounds/tick.ogg", Sound.class);
    }

    public void loadMusic() {
        manager.load("music/chase.ogg", Music.class);
    }

    public Sound tick() {
        return manager.get("sounds/tick.ogg", Sound.class);
    }

    public Sound ping() {
        return manager.get("sounds/ping.ogg", Sound.class);
    }

    public Sound death() {
        return manager.get("sounds/death.ogg", Sound.class);
    }

    public Sound jingle() {
        return manager.get("sounds/jingle1.ogg", Sound.class);
    }

    public Sound plop() {
        return manager.get("sounds/plop1.ogg", Sound.class);
    }

    public Sound boom() {
        return manager.get("sounds/boom.ogg", Sound.class);
    }

    public Music gameMusic() {
        return manager.get("music/chase.ogg", Music.class);
    }

    public Texture title() {
        return manager.get("ui/title.png", Texture.class);
    }

    public Texture tutBackground() {
        return manager.get("ui/tutBackground.png", Texture.class);
    }

    public Texture star() {
        return manager.get("ui/star.png", Texture.class);
    }

    public Texture progressBarContainer() {
        return manager.get("ui/progressBarContainer.png", Texture.class);
    }

    public Texture gameOver() {
        return manager.get("ui/gameOver.png", Texture.class);
    }

    public Texture progressBarBlue() {
        return manager.get("ui/progressBarBlue.png", Texture.class);
    }

    public Texture progressBarRed() {
        return manager.get("ui/progressBarRed.png", Texture.class);
    }

    public Texture progressBarDark() {
        return manager.get("ui/progressBarDark.png", Texture.class);
    }

    public Texture rays() {
        return manager.get("ui/rays.png", Texture.class);
    }

    public Texture raysRed() {
        return manager.get("ui/raysRed.png", Texture.class);
    }

    public Texture hudBoard() {
        return manager.get("ui/hudBoard.png", Texture.class);
    }

    public Texture quit() {
        return manager.get("ui/quit.png", Texture.class);
    }

    public Texture emptyGreenUnclicked() {
        return manager.get("ui/emptyGreenUnclicked.png", Texture.class);
    }

    public Texture emptyGreenClicked() {
        return manager.get("ui/emptyGreenClicked.png", Texture.class);
    }

    public Texture yesUnclicked() {
        return manager.get("ui/yesUnclicked.png", Texture.class);
    }

    public Texture infoClicked() {
        return manager.get("ui/infoClicked.png", Texture.class);
    }

    public Texture infoUnclicked() {
        return manager.get("ui/infoUnclicked.png", Texture.class);
    }

    public Texture yesClicked() {
        return manager.get("ui/yesClicked.png", Texture.class);
    }

    public Texture noUnclicked() {
        return manager.get("ui/noUnclicked.png", Texture.class);
    }

    public Texture noClicked() {
        return manager.get("ui/noClicked.png", Texture.class);
    }

    public Texture twitterUnclicked() {
        return manager.get("ui/twitterUnclicked.png", Texture.class);
    }

    public Texture twitterClicked() {
        return manager.get("ui/twitterClicked.png", Texture.class);
    }

    public Texture homeUnclicked() {
        return manager.get("ui/homeUnclicked.png", Texture.class);
    }

    public Texture homeClicked() {
        return manager.get("ui/homeClicked.png", Texture.class);
    }

    public Texture nextUnclicked() {
        return manager.get("ui/nextUnclicked.png", Texture.class);
    }

    public Texture nextClicked() {
        return manager.get("ui/nextClicked.png", Texture.class);
    }

    public Texture prevUnclicked() {
        return manager.get("ui/prevUnclicked.png", Texture.class);
    }

    public Texture prevClicked() {
        return manager.get("ui/prevClicked.png", Texture.class);
    }

    public Texture optionsUnclicked() {
        return manager.get("ui/optionsUnclicked.png", Texture.class);
    }

    public Texture optionsClicked() {
        return manager.get("ui/optionsClicked.png", Texture.class);
    }

    public Texture highScoreUnclicked() {
        return manager.get("ui/highScoreUnclicked.png", Texture.class);
    }

    public Texture highScoreClicked() {
        return manager.get("ui/highScoreClicked.png", Texture.class);
    }

    public Texture darkOverlay() {
        return manager.get("ui/blackScreen.png", Texture.class);
    }

    public Texture nextLevelMenuBackground() {
        return manager.get("ui/nextLevelMenuBackground.png", Texture.class);
    }

    public Texture highscoreBackground() {
        return manager.get("ui/highScoreBackground.png", Texture.class);
    }

    public Texture optionsBackground() {
        return manager.get("ui/optionsBackground.png", Texture.class);
    }

    public Texture creditsBackground() {
        return manager.get("ui/creditsBackground.png", Texture.class);
    }

    public Texture heart() {
        return manager.get("ui/heart.png", Texture.class);
    }

    public Texture emptyHeart() {
        return manager.get("ui/emptyHeart.png", Texture.class);
    }

    public Texture redOverlay() {
        return manager.get("images/redOverlay.png", Texture.class);
    }

    public Texture grass1() {
        return manager.get("images/grass1.png", Texture.class);
    }

    public Texture grass2() {
        return manager.get("images/grass2.png", Texture.class);
    }

    public Texture grass3() {
        return manager.get("images/grass3.png", Texture.class);
    }

    public Texture grassBlue1() {
        return manager.get("images/grassBlue1.png", Texture.class);
    }

    public Texture grassBlue2() {
        return manager.get("images/grassBlue2.png", Texture.class);
    }

    public Texture grassBlue3() {
        return manager.get("images/grassBlue3.png", Texture.class);
    }

    public Texture grassPink1() {
        return manager.get("images/grassPink1.png", Texture.class);
    }

    public Texture grassPink2() {
        return manager.get("images/grassPink2.png", Texture.class);
    }

    public Texture grassPink3() {
        return manager.get("images/grassPink3.png", Texture.class);
    }

    public Texture greenRock() {
        return manager.get("images/blocks/green_rock.png", Texture.class);
    }

    public Texture brownRock() {
        return manager.get("images/blocks/brown_rock.png", Texture.class);
    }

    public Texture purpleRock() {
        return manager.get("images/blocks/purple_rock.png", Texture.class);
    }

    public Texture redRock() {
        return manager.get("images/blocks/red_rock.png", Texture.class);
    }

    public Texture yellowRock() {
        return manager.get("images/blocks/yellow_rock.png", Texture.class);
    }

    public Texture orangeRock() {
        return manager.get("images/blocks/orange_rock.png", Texture.class);
    }

    public Texture blueRock() {
        return manager.get("images/blocks/blue_rock.png", Texture.class);
    }

    public Texture allBlocks() {
        return manager.get("images/blocks/allBlocks.png", Texture.class);
    }

    public Texture fireball() {
        return manager.get("images/fireball.png", Texture.class);
    }

    public Texture fireballRed() {
        return manager.get("images/fireballRed.png", Texture.class);
    }

    public Texture fireballGreen() {
        return manager.get("images/fireballGreen.png", Texture.class);
    }

    public Texture turret() {
        return manager.get("images/turret1.png", Texture.class);
    }

    public Texture turretBase() {
        return manager.get("images/turretBase.png", Texture.class);
    }

    public void queueAddSkin() {
//        SkinParameter params = new SkinParameter("skin/glassy-ui.atlas");
//        manager.load(skin, Skin.class, params);
    }

    public void queueAddFonts() {
    }

    public AssetManager getManager() {
        return manager;
    }
}
