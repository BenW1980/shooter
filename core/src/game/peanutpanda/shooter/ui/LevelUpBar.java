package game.peanutpanda.shooter.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.peanutpanda.shooter.assetmanager.AssetLoader;
import game.peanutpanda.shooter.gameobjects.Player;
import game.peanutpanda.shooter.states.ScreenSize;


public class LevelUpBar {

    public static float MAX_WIDTH_BAR = 218.0f;
    public static float PART_BAR = 38.0f;
    private AssetLoader assetLoader;
    private SpriteBatch batch;
    private float currentBarWidth;
    private Sprite bar;
    private Sprite barBlue;
    private Sprite barFlash;
    private Sprite barDark;
    private boolean levelledUp;
    private BitmapFont fontWolf;
    private Player player;
    private float levelBarTime;
    private int timesFlashed;

    private float barHeight;
    private float barYpos;

    public LevelUpBar(SpriteBatch batch, AssetLoader assetLoader, Player player, BitmapFont fontWolf) {
        this.assetLoader = assetLoader;
        this.batch = batch;
        this.player = player;
        this.barBlue = new Sprite(assetLoader.progressBarBlue());
        this.barFlash = new Sprite(assetLoader.progressBarRed());
        this.barDark = new Sprite(assetLoader.progressBarDark());
        this.bar = barBlue;
        this.fontWolf = fontWolf;
        currentBarWidth = 15;

        barHeight = 34;
        barYpos = ScreenSize.HEIGHT.getSize() / 2 - 99;
    }

    public void draw(float levelCount) {

        batch.draw(assetLoader.progressBarContainer(), ScreenSize.WIDTH.getSize() / 2 - 155, ScreenSize.HEIGHT.getSize() / 2 - 120, 320, 75);
        batch.draw(bar, ScreenSize.WIDTH.getSize() / 2 - 77, barYpos, currentBarWidth, barHeight);
        fontWolf.draw(batch, "" + player.getSkilLevel(), ScreenSize.WIDTH.getSize() / 2 - 124, ScreenSize.HEIGHT.getSize() / 2 - 61);

        if (levelCount < 6) {
            levelledUp = false;
            if (currentBarWidth < PART_BAR * levelCount) {
                currentBarWidth += Gdx.graphics.getDeltaTime() * 75;
            }

        } else if (levelCount == 6) {
            levelUp();

        } else {
            if (currentBarWidth < PART_BAR * 1.0) {
                currentBarWidth += Gdx.graphics.getDeltaTime() * 75;
            }
        }
    }

    public void levelUp() {

        if (!levelledUp) {
            currentBarWidth += Gdx.graphics.getDeltaTime() * 75;

            if (currentBarWidth > MAX_WIDTH_BAR) {
                currentBarWidth = MAX_WIDTH_BAR;
                bar = barFlash;

                if (timesFlashed < 10) {
                    levelBarTime += Gdx.graphics.getDeltaTime() * 10;
                    if (levelBarTime > 0.50f) {
                        bar = barDark;
                        timesFlashed++;
                        levelBarTime = 0;
                    }
                } else {
                    bar = barBlue;
                    levelledUp = true;
                    player.levelUp();
                    currentBarWidth = 0;
                }
            }
        }

        if (levelledUp) {
            timesFlashed = 0;
            levelBarTime = 0;
            if (currentBarWidth < 15.0f) {
                currentBarWidth += Gdx.graphics.getDeltaTime() * 75;
            }
        }
    }

    public void setCurrentBarWidth(float currentBarWidth) {
        this.currentBarWidth = currentBarWidth;
    }
}
