package game.peanutpanda.shooter.ui;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.viewport.Viewport;

import game.peanutpanda.shooter.AudioManager;
import game.peanutpanda.shooter.assetmanager.AssetLoader;
import game.peanutpanda.shooter.controller.Controller;
import game.peanutpanda.shooter.states.ScreenSize;

public class ConfirmQuitWindow {

    private AssetLoader assetLoader;
    private SpriteBatch batch;
    private Button yesButton;
    private Button noButton;
    private AudioManager audioManager;

    public ConfirmQuitWindow(AssetLoader assetLoader, SpriteBatch batch, Controller controller, Viewport viewport, AudioManager audioManager) {
        this.audioManager = audioManager;
        this.assetLoader = assetLoader;
        this.batch = batch;

        int btnSize = 90;

        this.yesButton = new Button(controller, viewport, batch,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 110, ScreenSize.HEIGHT.getSize() / 2 - 123, btnSize, btnSize),
                assetLoader.yesUnclicked(), assetLoader.yesClicked());

        this.noButton = new Button(controller, viewport, batch,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 30, ScreenSize.HEIGHT.getSize() / 2 - 123, btnSize, btnSize),
                assetLoader.noUnclicked(), assetLoader.noClicked());
    }

    public void draw() {
        batch.draw(assetLoader.darkOverlay(), -100, -100, ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() * 2);
        batch.draw(assetLoader.quit(), ScreenSize.WIDTH.getSize() / 2 - 180, ScreenSize.HEIGHT.getSize() / 2 - 150, 357, 252);

        yesButton.draw(audioManager);
        noButton.draw(audioManager);
    }

    public Button getYesButton() {
        return yesButton;
    }

    public Button getNoButton() {
        return noButton;
    }


}
