package game.peanutpanda.shooter.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.viewport.Viewport;

import game.peanutpanda.shooter.AudioManager;
import game.peanutpanda.shooter.GameData;
import game.peanutpanda.shooter.assetmanager.AssetLoader;
import game.peanutpanda.shooter.controller.Controller;
import game.peanutpanda.shooter.gameobjects.Player;
import game.peanutpanda.shooter.states.ScreenSize;

public class NextLevelWindow {

    private LevelUpBar levelUpBar;
    private Button nextLevelButton;
    private Button mainMenuButton;
    private Player player;
    private AssetLoader assetLoader;
    private SpriteBatch batch;
    private Controller controller;
    private BitmapFont fontWolf;
    private Sprite rays;
    private Sprite star;
    private float timePassed;
    private float raySize = 800;
    private boolean upgradeFinished;
    private boolean hasExploSoundPlayed;

    public NextLevelWindow(AssetLoader assetLoader, SpriteBatch batch, Controller controller,
                           Viewport viewport, Player player, BitmapFont fontWolf) {
        this.assetLoader = assetLoader;
        this.controller = controller;
        this.batch = batch;
        this.fontWolf = fontWolf;
        this.player = player;
        this.rays = new Sprite(assetLoader.rays());
        this.star = new Sprite(assetLoader.star());
        int btnSize = 80;
        this.nextLevelButton = new Button(controller, viewport, batch,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 20, 145, btnSize, btnSize),
                assetLoader.nextUnclicked(), assetLoader.nextClicked());

        this.mainMenuButton = new Button(controller, viewport, batch,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 100, 145, btnSize, btnSize),
                assetLoader.homeUnclicked(), assetLoader.homeClicked());

        this.levelUpBar = new LevelUpBar(batch, assetLoader, player, fontWolf);
    }

    public void draw(float levelCount, GameData gameData, AudioManager audioManager) {

        gameData.saveHighscore(player.getScore());

        upgradeFinished = false;

        batch.draw(assetLoader.darkOverlay(), -100, -100, ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() * 2);

        timePassed += Gdx.graphics.getDeltaTime();
        rays.setRotation(timePassed * 100);
        rays.setPosition(ScreenSize.WIDTH.getSize() / 2 - (rays.getWidth() / 2), ScreenSize.HEIGHT.getSize() / 2 - (rays.getHeight() / 2));
        if (rays.getWidth() < raySize) {
            rays.setSize(timePassed * 2000, timePassed * 2000);
        }
        this.rays.setOriginCenter();
        rays.draw(batch);

        if (rays.getWidth() > raySize - 100) {
            batch.draw(assetLoader.nextLevelMenuBackground(), 5, 170, 470, 550);

            fontWolf.draw(batch, "" + String.format("%,d", player.getScore()), ScreenSize.WIDTH.getSize() / 2 - 15, ScreenSize.HEIGHT.getSize() / 2 + 165);
            fontWolf.draw(batch, "" + String.format("%,d", gameData.highScore()), ScreenSize.WIDTH.getSize() / 2 - 15, ScreenSize.HEIGHT.getSize() / 2 + 70);
            checkTurnsToUpgrade(levelCount, audioManager);
            levelUpBar.draw(levelCount);

            if (player.getScore() == gameData.highScore()) {
                drawStar();
            }

            if (timePassed > 1f && upgradeFinished) {
                nextLevelButton.draw(audioManager);
                mainMenuButton.draw(audioManager);
            } else {
                controller.touchLocation.setZero();
            }
        }
    }

    private void drawStar() {

        star.setPosition(ScreenSize.WIDTH.getSize() / 2 + 127, ScreenSize.HEIGHT.getSize() / 2 + 32);
        star.setSize(30, 33);
        this.star.setOriginCenter();
        star.draw(batch);

    }

    private void checkTurnsToUpgrade(float levelCount, AudioManager audioManager) {

        float result = 0;

        if (levelCount < 6) {
            result = 6 - levelCount;
        } else if (levelCount > 6 && levelCount < 13) {
            result = 13 - levelCount;
        } else if (levelCount > 13 && levelCount < 20) {
            result = 20 - levelCount;
        } else if (levelCount > 20 && levelCount < 27) {
            result = 27 - levelCount;
        } else if (levelCount > 27 && levelCount < 34) {
            result = 34 - levelCount;
        } else if (levelCount > 34 && levelCount < 41) {
            result = 41 - levelCount;
        }

        if (result == 0) {
            if (!hasExploSoundPlayed) {
                audioManager.playJingle();
                hasExploSoundPlayed = true;
            }
            if (player.getTimesHeartAdded() == Player.MAX_HEARTS_TO_ADD) {
                upgradeFinished = true;
            }
            player.fillHeartsAfterUpgrade(assetLoader);
        } else {
            hasExploSoundPlayed = false;
        }

        upgradeFinished = true;

    }

    public Button getNextLevelButton() {
        return nextLevelButton;
    }

    public LevelUpBar getLevelUpBar() {
        return levelUpBar;
    }

    public Button getMainMenuButton() {
        return mainMenuButton;
    }
}
