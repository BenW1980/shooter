package game.peanutpanda.shooter.ui;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.Viewport;

import game.peanutpanda.shooter.AudioManager;
import game.peanutpanda.shooter.controller.Controller;

public class Button {

    protected Controller controller;
    protected Viewport viewport;
    protected SpriteBatch batch;
    protected Rectangle rectangle;
    protected Texture texture, unclickedTexture, clickedTexture;
    protected boolean drawn;
    private boolean processed;

    public Button(Controller controller, Viewport viewport, SpriteBatch batch,
                  Rectangle rectangle, Texture unclickedTexture, Texture clickedTexture) {
        this.controller = controller;
        this.viewport = viewport;
        this.batch = batch;
        this.rectangle = rectangle;
        this.unclickedTexture = unclickedTexture;
        this.clickedTexture = clickedTexture;
        this.processed = false;
    }

    protected Vector3 getTouchVector() {
        Vector3 touchPos = new Vector3(controller.touchLocation, 0);
        viewport.getCamera().unproject(touchPos,
                viewport.getScreenX(),
                viewport.getScreenY(),
                viewport.getScreenWidth(),
                viewport.getScreenHeight());
        return touchPos;
    }

    public boolean isPushed(Vector3 touchPos) {
        return rectangle.contains(touchPos.x, touchPos.y) && controller.isTouched;
    }

    public boolean isReleasedAfterPushed(Vector3 touchPos) {
        return rectangle.contains(touchPos.x, touchPos.y) && !controller.isTouched;
    }

    public void draw(AudioManager audioManager) {

        Vector3 touchPos = getTouchVector();

        if (drawn && isReleasedAfterPushed(touchPos)) {
            audioManager.playBtnSound();
            controller.touchLocation.setZero();
            processed = true;
        }

        texture = isPushed(touchPos) ? clickedTexture : unclickedTexture;

        batch.draw(texture, rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        drawn = true;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }
}
