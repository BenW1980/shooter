package game.peanutpanda.shooter.ui;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.Viewport;

import game.peanutpanda.shooter.AudioManager;
import game.peanutpanda.shooter.controller.Controller;


public class ToggleButton extends Button {

    private Texture toggledUnclickedTexture, toggledClickedTexture;
    private boolean toggled;

    public ToggleButton(Controller controller, Viewport viewport, SpriteBatch batch, Rectangle rectangle,
                        Texture unclickedTexture, Texture clickedTexture,
                        Texture toggledUnclickedTexture, Texture toggledClickedTexture) {

        super(controller, viewport, batch, rectangle, unclickedTexture, clickedTexture);
        this.toggledUnclickedTexture = toggledUnclickedTexture;
        this.toggledClickedTexture = toggledClickedTexture;
    }

    public void draw(AudioManager audioManager) {

        Vector3 touchPos = getTouchVector();
        if (drawn && isReleasedAfterPushed(touchPos)) {
            audioManager.playTglSound();
            controller.touchLocation.setZero();
            toggled = !toggled;
        }

        texture = isPushed(touchPos) ?
                toggled ? toggledClickedTexture : clickedTexture :
                toggled ? toggledUnclickedTexture : unclickedTexture;

        batch.draw(texture, rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        drawn = true;
    }

    public boolean isToggled() {
        return toggled;
    }

    public void setToggled(boolean toggled) {
        this.toggled = toggled;
    }
}
