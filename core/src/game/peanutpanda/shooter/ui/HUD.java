package game.peanutpanda.shooter.ui;


import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

import game.peanutpanda.shooter.assetmanager.AssetLoader;
import game.peanutpanda.shooter.gameobjects.Block;
import game.peanutpanda.shooter.gameobjects.Player;
import game.peanutpanda.shooter.states.BattleState;
import game.peanutpanda.shooter.states.ScreenSize;

public class HUD {

    private Player player;
    private AssetLoader assetLoader;
    private SpriteBatch batch;
    private BitmapFont font;

    public HUD(Player player, AssetLoader assetLoader, SpriteBatch batch, BitmapFont font) {
        this.player = player;
        this.assetLoader = assetLoader;
        this.batch = batch;
        this.font = font;
    }

    public void draw(Array<Block> blocks, BattleState battleState) {
        batch.draw(assetLoader.hudBoard(), 0, ScreenSize.HEIGHT.getSize() - 85, ScreenSize.WIDTH.getSize(), 85);

        for (int i = 0; i < Player.MAX_LIFE; i++) {
            batch.draw(
                    assetLoader.emptyHeart(), 35 + (i * 30),
                    ScreenSize.HEIGHT.getSize() - 35, 20, 20);
        }

        for (int i = 0; i < player.getHeartArray().size; i++) {
            batch.draw(
                    player.getHeartArray().get(i),
                    35 + (i * 30), ScreenSize.HEIGHT.getSize() - 35,
                    player.getHeartArray().get(i).getWidth(),
                    player.getHeartArray().get(i).getHeight());
        }

        font.draw(batch, "Score : " + String.format("%,d", player.getScore()), 30, ScreenSize.HEIGHT.getSize() - 45);

        if (battleState == BattleState.LAND_BATTLE) {
            font.draw(batch, "Blocks : " + blocks.size, 310, ScreenSize.HEIGHT.getSize() - 45);
        } else if (battleState == BattleState.SPACE_BATTLE) {
            font.draw(batch, player.getEnemiesKilled() + " / 50", 355, ScreenSize.HEIGHT.getSize() - 45);
        }

    }
}
