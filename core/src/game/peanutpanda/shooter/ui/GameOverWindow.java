package game.peanutpanda.shooter.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.viewport.Viewport;

import game.peanutpanda.shooter.AudioManager;
import game.peanutpanda.shooter.GameData;
import game.peanutpanda.shooter.assetmanager.AssetLoader;
import game.peanutpanda.shooter.controller.Controller;
import game.peanutpanda.shooter.gameobjects.Player;
import game.peanutpanda.shooter.states.ScreenSize;

public class GameOverWindow {

    private Button mainMenuButton;
    private Player player;
    private AssetLoader assetLoader;
    private SpriteBatch batch;
    private Controller controller;
    private BitmapFont fontWolf;
    private float timePassed;
    private Sprite rays;
    private Sprite star;
    private float raySize = 800;
    private AudioManager audioManager;

    public GameOverWindow(AssetLoader assetLoader, SpriteBatch batch, Controller controller,
                          Viewport viewport, Player player, BitmapFont fontWolf, AudioManager audioManager) {
        this.assetLoader = assetLoader;
        this.controller = controller;
        this.audioManager = audioManager;
        this.batch = batch;
        this.fontWolf = fontWolf;
        this.player = player;
        this.rays = new Sprite(assetLoader.raysRed());
        this.star = new Sprite(assetLoader.star());
        this.mainMenuButton = new Button(controller, viewport, batch,
                new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 45, 145, 90, 90),
                assetLoader.homeUnclicked(), assetLoader.homeClicked());
    }

    public void draw(GameData gameData) {

        gameData.saveHighscore(player.getScore());

        batch.draw(assetLoader.darkOverlay(), -100, -100, ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() * 2);

        timePassed += Gdx.graphics.getDeltaTime();
        rays.setRotation(timePassed * 100);
        rays.setPosition(ScreenSize.WIDTH.getSize() / 2 - (rays.getWidth() / 2), ScreenSize.HEIGHT.getSize() / 2 - (rays.getHeight() / 2));
        if (rays.getWidth() < raySize) {
            rays.setSize(timePassed * 2000, timePassed * 2000);
        }
        this.rays.setOriginCenter();
        rays.draw(batch);

        if (rays.getWidth() > raySize - 100) {
            batch.draw(assetLoader.gameOver(), 5, 170, 470, 550);

            fontWolf.draw(batch, "" + String.format("%,d", player.getScore()), ScreenSize.WIDTH.getSize() / 2 - 15, ScreenSize.HEIGHT.getSize() / 2 + 47);
            fontWolf.draw(batch, "" + String.format("%,d", gameData.highScore()), ScreenSize.WIDTH.getSize() / 2 - 15, ScreenSize.HEIGHT.getSize() / 2 - 39);

            if (player.getScore() == gameData.highScore()) {
                drawStar();
            }

            if (timePassed > 1f) {
                mainMenuButton.draw(audioManager);
            } else {
                controller.touchLocation.setZero();
            }
        }
    }

    private void drawStar() {

        star.setPosition(ScreenSize.WIDTH.getSize() / 2 + 127, ScreenSize.HEIGHT.getSize() / 2 - 77);
        star.setSize(30, 33);
        this.star.setOriginCenter();
        star.draw(batch);

    }


    public Button getMainMenuButton() {
        return mainMenuButton;
    }
}
