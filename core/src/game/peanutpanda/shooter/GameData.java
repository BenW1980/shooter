package game.peanutpanda.shooter;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class GameData {

    private Preferences prefs;

    public GameData() {
        prefs = Gdx.app.getPreferences("game-prefs");
    }

    public void saveHighscore(int score) {
        if (score > highScore()) {
            prefs.putInteger("highscore", score);
            prefs.flush();
        }
    }

    public int highScore() {
        return prefs.getInteger("highscore");
    }

    public void saveMusicPref(boolean music) {
        prefs.putBoolean("music", music);
        prefs.flush();
    }

    public boolean musicIsOn() {
        return prefs.getBoolean("music", true);
    }

    public void saveSoundPref(boolean sound) {
        prefs.putBoolean("sound", sound);
        prefs.flush();
    }

    public boolean soundIsOn() {
        return prefs.getBoolean("sound", true);
    }

    public void saveScreenShakePref(boolean shake) {
        prefs.putBoolean("shake", shake);
        prefs.flush();
    }

    public boolean screenShakeIsOn() {
        return prefs.getBoolean("shake", true);
    }

    public void saveExplosionSizePref(boolean explo) {
        prefs.putBoolean("explo", explo);
        prefs.flush();
    }

    public boolean hugeExplosionsAreOn() {
        return prefs.getBoolean("explo", false);
    }
}