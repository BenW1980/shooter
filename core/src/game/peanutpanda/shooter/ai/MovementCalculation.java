package game.peanutpanda.shooter.ai;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import game.peanutpanda.shooter.gameobjects.MovingGameObject;
import game.peanutpanda.shooter.states.ScreenSize;

public class MovementCalculation {

    public void calculateMovementToCoords(Vector2 dir, Rectangle rectangle, float x, float y) {
        dir.x = x - rectangle.x;
        dir.y = y - rectangle.y;
        double hyp = Math.sqrt(dir.x * dir.x + dir.y * dir.y);
        dir.x /= hyp;
        dir.y /= hyp;
    }

    public void goDown(Rectangle rectangle, float speed, float delta) {
        rectangle.y -= speed * delta;
    }

    public void goLeft(Rectangle rectangle, float speed, float delta) {
        rectangle.x -= speed * delta;
    }

    public void goRight(Rectangle rectangle, float speed, float delta) {
        rectangle.x += speed * delta;
    }

    public void chasePlayer(Vector2 dir, Rectangle rectangle, float speed, float x, float y) {
        calculateMovementToCoords(dir, rectangle, x, y);
        rectangle.x += dir.x * speed;
        rectangle.y += dir.y * speed;
    }

    public void moveAwayFromPlayer(Vector2 dir, Rectangle rectangle, float speed, float x, float y) {
        calculateMovementToCoords(dir, rectangle, x, y);
        rectangle.x -= dir.x * speed;
        rectangle.y -= dir.y * speed;
    }

    public void sinusoidPowerup(MovingGameObject obj, float delta) {

        obj.setNumberOfTicks(obj.getNumberOfTicks() + 1);

        obj.getRectangle().y = (float) ((150 * Math.sin(obj.getNumberOfTicks() * 1.5 * Math.PI / 100)) + 600);

        if (obj.getRectangle().x > ScreenSize.WIDTH.getSize() - obj.getRectangle().width) {
            obj.setSpeed(-(MathUtils.random(750, 1250)));
        } else if (obj.getRectangle().x < 0) {
            obj.setSpeed(MathUtils.random(750, 1250));
        }
        obj.getRectangle().x += obj.getSpeed() * delta;
    }

}
